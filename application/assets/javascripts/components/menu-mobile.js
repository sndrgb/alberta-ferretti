import { TimelineMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'

import Panel from '../components/Panel'
import DropdownController from './dropdown-controller'
import RafScroll from './scrollbar/scroll'
import { qsa, qs, hasClass, toggleClass, addClass, removeClass, setBodyFixed } from '../base/utils'

class MenuMobile extends Component {

  afterInit() {
    this.$els.toggles = qsa('[data-toggle-menu-mobile]')
    this.$els.voices = qsa('[data-voice]', this.$el)

    qsa('[data-menu-block]').map(el => el.setAttribute('data-dropdown', ''))
    this.dropdownController = new DropdownController(qs('#menu', this.$el), {
      selectors: '[data-voice]'
    }).init()
    this.setEvents()

    this.setRef({
      id: 'panel',
      component: Panel,
      el: qs('[data-mobile-menu-panel]', this.$el),
      opts: {
        isChild: true
      }
    })

    this.on('change:isOpen', (isOpen) => {
      if (isOpen) {
        setBodyFixed(true)
        addClass(document.body, 'is-overlay')
        this.open()
      } else {
        setBodyFixed(false)
        removeClass(document.body, 'is-overlay')
        this.close()
      }
    })

    RafScroll.add(this.scrollHandle.bind(this))
  }

  setEvents() {
    this.$els.toggles.forEach(el => {
      this.$ev.on(el, 'click', () => {
        if (this.getState('isAnimating')) return
        this.setState('isAnimating', true)
        this.setState('isOpen', !this.getState('isOpen'))
      })
    })
  }

  open() {
    const tl = new TimelineMax({ onComplete: () => { this.setState('isAnimating', false) } })

    tl.set(this.$el, { autoAlpha: 1 })
    .fromTo(this.$el, 1, {
      xPercent: -100,
      className: '+=is-open'
    }, {
      xPercent: 0,
      clearProps: 'transform',
      ease: Expo.easeInOut
    })
  }

  close() {
    const tl = new TimelineMax({ onComplete: () => { this.setState('isAnimating', false) } })
    this.$refs.panel.setState('isModalOpen', false)

    tl
      .to(this.$el, 1, {
        xPercent: -100,
        ease: Expo.easeInOut
      })
      .set(this.$el, {
        className: '-=is-open',
        clearProps: 'all'
      })
  }

  getInitialState() {
    return {
      isOpen: false
    }
  }

  scrollHandle() {
    // menu directions
    if (this.$el && !this.getState('isOpen')) {
      if (
        hasClass(document.body, 'is-home') ||
        hasClass(document.body, 'is-collection') ||
        hasClass(document.body, 'is-mood') ||
        hasClass(document.body, 'is-adcampaign')
      ) {
        if (window.scrollY > 600 && hasClass(document.body, 'is-header-light')) {
          toggleClass(document.body, 'is-header-light')
        } else if (window.scrollY === 0 && !hasClass(document.body, 'is-header-light')) {
          addClass(document.body, 'is-header-light')
        }
      }
    }
  }
}

export default MenuMobile