import { TimelineMax, Expo, Power2, TweenMax } from 'gsap/all'
import SplitText from '../../../vendors/gsap/commonjs-flat/SplitText'

const animations = new Map()
const lerp = (a, b, n) => ((1 - n) * a) + (n * b)

animations.set('image', (el) => {
  const image = el.querySelector('[data-image]')
  const tl = new TimelineMax()

  tl
  .from(image, 2, {
    scale: 1.2,
    autoAlpha: 0,
    ease: Expo.easeOut,
    clearProps: 'transform, visibility, opacity'
  })
})

animations.set('top', (el) => {
  const tl = new TimelineMax()

  tl
  .fromTo(el, 1.2, {
    yPercent: 20,
    autoAlpha: 0
  }, {
    autoAlpha: 1,
    yPercent: 0,
    ease: Expo.easeInOut,
    clearProps: 'transform, visibility, opacity'
  })
})

animations.set('text', (el) => {
  const tl = new TimelineMax()
  const split = new SplitText(el, { type: 'lines, words', linesClass: 'lines' })

  tl
  .staggerFrom(split.lines, 1, {
    y: 45,
    autoAlpha: 0,
    delay: 0.2,
    ease: Power2.easeOut,
    clearProps: 'all'
  }, 0.08)
  .set(el, { className: '+=is-visible' })
  // .add(() => { split.revert() })
})

animations.set('skew', (el) => {
  const tl = new TimelineMax()
  const split = new SplitText(el, { type: 'lines, words', linesClass: 'lines' })

  tl
  .staggerFrom(split.lines, 0.8, {
    skewX: -35,
    delay: 0.5,
    ease: Power2.easeOut,
    autoAlpha: 0
    // x: 10,
  }, 0.05)
  .set(el, { className: '+=is-visible' })
  .add(() => { split.revert() })
})

animations.set('fade', (el) => {
  TweenMax.to(el, 1.4, {
    autoAlpha: 1,
    delay: 0.3,
    ease: Expo.easeInOut
  })
})

animations.set('scale', (el) => {
  TweenMax.from(el, 0.6, {
    scale: 0,
    delay: 0.2,
    ease: Power2.easeOut,
    clearProps: 'transform'
  })
})

animations.set('blur', (el) => {
  const tl = new TimelineMax()
  const split = new SplitText(el, { type: 'lines', linesClass: 'lines' })

  tl
    .set(el, { autoAlpha: 1 })
    .staggerFromTo(split.lines, 1.5, {
      autoAlpha: 0,
      y: 20,
      filter: 'blur(30px)'
    }, {
      autoAlpha: 1,
      y: 0,
      filter: 'blur(0px)'
    }, 0.03)
    .add(() => { split.revert() })
})

const offsets = []
animations.set('parallax', (el, i) => {
  const bound = el.parentElement.getBoundingClientRect().top
  let final = 1

  if (!el.getAttribute('data-animated')) {
    el.setAttribute('data-animated', true)
    offsets[i] = bound
  }

  const parallaxAttr = el.getAttribute('data-parallax')
  const elH = el.parentElement.clientHeight
  const relativity = parallaxAttr === 'relative' ? offsets[i] : ((window.innerHeight - elH) / 2)
  const offsetTop = bound - relativity
  const coefficent = parallaxAttr !== null && parallaxAttr !== 'relative' ? parallaxAttr : 0.2
  const maxY = Math.max(-window.innerHeight, offsetTop)
  const baseTransform = Math.floor(Math.min(window.innerHeight, maxY), 2)
  const translateY = baseTransform * parseFloat(coefficent)

  final = lerp(final, translateY, 1)
  TweenMax.set(el, { y: final })
})

export default animations