import { TweenMax, TimelineMax, Expo, Power2 } from 'gsap/all'
import SplitText from '../../../vendors/gsap/commonjs-flat/SplitText'

const setters = new Map()

setters.set('image', (el) => {
  const image = el.querySelector('[data-image]')
  const tl = new TimelineMax()

  tl
  .from(image, 2, {
    scale: 1.2,
    autoAlpha: 0,
    ease: Expo.easeOut,
    clearProps: 'transform, visibility, opacity'
  })
})

setters.set('top', (el) => {
  TweenMax.set(el, { autoAlpha: 0 })
})

setters.set('text', (el) => {
  const tl = new TimelineMax()
  const split = new SplitText(el, { type: 'lines, words', linesClass: 'lines' })

  tl
  .staggerFrom(split.lines, 1, {
    y: 45,
    autoAlpha: 0,
    delay: 0.2,
    ease: Power2.easeOut,
    clearProps: 'all'
  }, 0.08)
  .set(el, { className: '+=is-visible' })
  // .add(() => { split.revert() })
})

setters.set('skew', (el) => {
  const tl = new TimelineMax()
  const split = new SplitText(el, { type: 'lines, words', linesClass: 'lines' })

  tl
  .staggerFrom(split.lines, 0.8, {
    skewX: -35,
    delay: 0.5,
    ease: Power2.easeOut,
    autoAlpha: 0
    // x: 10,
  }, 0.05)
  .set(el, { className: '+=is-visible' })
  .add(() => { split.revert() })
})

setters.set('fade', (el) => {
  TweenMax.set(el, { autoAlpha: 0 })
})

setters.set('scale', (el) => {
  TweenMax.from(el, 0.6, {
    scale: 0,
    delay: 0.2,
    ease: Power2.easeOut,
    clearProps: 'transform'
  })
})

setters.set('blur', (el) => {
  TweenMax.set(el, { autoAlpha: 0 })
})

export default setters