// import { addClass } from '../base/utils'

const TextSearch = (() => {
  const search = document.querySelectorAll('.test-search-input')
  const result = document.querySelectorAll('.test-search-results')

  search.forEach(el => {
    el.addEventListener('keyup', () => {
      if (el.value.length > 2) {
        result.forEach(res => res.removeAttribute('style'))
      } else {
        result.forEach(res => res.setAttribute('style', 'display: none'))
      }
    })
  })
})()

export default TextSearch