import { TimelineMax, Expo } from 'gsap/all'

import Modal from './modal'
import { qsa, qs } from '../base/utils'

class Newsletter extends Modal {

  mounted() {
    super.mounted()

    this.$els.innerWrapper = qs('.js-inner', this.$el)
    this.$els.triggers = qsa('[data-trigger="newsletter"]')

    this.setEvents()
  }

  getInitialState() {
    return {
      isModalOpen: false
    }
  }

  setEvents() {
    this.$els.triggers.forEach(el => {
      this.$ev.on(el, 'click', this.trigger.bind(this))
    })
  }

  trigger() {
    this.setState('isModalOpen', !this.getState('isModalOpen'))
  }

  show() {
    super.show()

    const tl = new TimelineMax()
    const $img = qs('img', this.$el)

    tl
    .set(this.$el, {
      className: '+=is-open'
    })
    .add('start')
    .fromTo(this.$el, 0.5, {
      autoAlpha: 0
    }, {
      autoAlpha: 1
    }, 'start')
    .fromTo($img, 2, {
      scale: 1.1
    }, {
      scale: 1,
      ease: Expo.easeInOut
    }, 'start')
    .fromTo(this.$els.innerWrapper, 2, {
      rotationY: 15,
      rotationX: -15,
      autoAlpha: 0
    }, {
      rotationY: 0,
      rotationX: 0,
      autoAlpha: 1,
      ease: Expo.easeInOut
    }, 'start')
  }

  hide() {
    super.hide()
    const tl = new TimelineMax()

    tl
    .add('start')
    .to(this.$els.innerWrapper, 1.5, {
      rotationY: 15,
      rotationX: -15,
      autoAlpha: 0,
      ease: Expo.easeInOut
    }, 'start')
    .to(this.$el, 1, {
      autoAlpha: 0
    }, 'start')
    .set(this.$el, {
      className: '-=is-open'
    })
  }
}

export default Newsletter