import { TweenMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'

import { qsa, stringToDOM } from '../base/utils'

class MenuBoutiques extends Component {

  mounted() {
    this.$els.locations = qsa('[data-location]', this.$el)

    this.setEvents()
  }

  setEvents() {
    this.$els.locations.forEach(el => {
      this.$ev.on(el, 'mouseenter', this.mouseEnter.bind(this, el))
      this.$ev.on(el, 'mouseleave', this.mouseLeave.bind(this, el))
    })
  }

  mouseEnter(el) {
    const bg = el.getAttribute('data-location')
    const $currentNav = this.$el.parentNode.parentNode

    this.$els.background = stringToDOM(`<div data-hide class="boutiques-background" style="background-image: url(${bg})"></div>`)
    $currentNav.appendChild(this.$els.background)

    this.enterBg(this.$els.background).play()
  }

  enterBg(el) {
    return TweenMax.to(el, 1.4, {
      paused: true,
      autoAlpha: 1,
      scale: 1,
      ease: Expo.easeInOut
    })
  }

  mouseLeave() {
    const bg = this.$els.background

    TweenMax.to(bg, 1.4, {
      autoAlpha: 0,
      scale: 1.2,
      ease: Expo.easeInOut,
      onComplete: () => {
        this.$el.parentNode.parentNode.removeChild(bg)
      }
    })
  }
}

export default MenuBoutiques