import { TweenMax, Power2, Expo, TimelineMax } from 'gsap/all'
import { Component } from 'yuzu'

import MenuBoutiques from '../components/menu-boutiques'
import MenuLanguages from '../components/menu-languages'
import RafScroll from './scrollbar/scroll'
import { qsa, qs, handleEvent, hasClass, addClass, setBodyFixed, toggleClass, stringToDOM } from '../base/utils'

class Menu extends Component {

  afterInit() {
    this.$els.voices = qsa('[data-voice]', this.$el)
    this.$els.menus = qsa('[data-menu]', this.$el)

    this.$els.hoverSlider = qs('#hover-bar', this.$el)
    this.$els.backLayer = qs('#back-layer', this.$el)

    this.hoverSliderLeft = this.$els.hoverSlider.getBoundingClientRect().left

    this.$els.closeButton = stringToDOM(`
      <button id="close-menu" class="c-close-button">
        <span></span>
        <span></span>
      </button>
    `)

    this.setMenuPosition()
    this.setEvents()
    this.mouseenter(this.$els.voices[0])

    const menuBoutiques = qs('#menu-boutiques', this.$el)
    if (menuBoutiques) {
      this.setRef({
        id: 'boutiques',
        component: MenuBoutiques,
        el: menuBoutiques
      })
    }

    const menuLanguages = qs('#menu-languages', this.$el)
    if (menuLanguages) {
      this.setRef({
        id: 'languages',
        component: MenuLanguages,
        el: menuLanguages
      })
    }

    RafScroll.add(this.menuScroll.bind(this))

    this.on('change:isOpen', val => {
      if (val) {
        setBodyFixed(true)
        this.appendCloseButton()

      } else {
        setBodyFixed(false)


        if (this.currentMenu) {
          this.$el.removeChild(this.$els.closeButton)
          this.handleClose()
        }
      }
    })
  }

  appendCloseButton() {
    if (this.currentMenu) {
      this.$el.appendChild(this.$els.closeButton)
      this.handleClose = this.$ev.on(this.$els.closeButton, 'click', this.closeMenu.bind(this))
    }
  }

  getInitialState() {
    return {
      isOpen: false,
      currentIndex: null,
      isAnimating: false
    }
  }

  setMenuPosition() {
    this.$els.menus.forEach(el => {
      const blocks = this.getBlocks(el)
      const content = this.getContainer(el)

      if (blocks.length === 1) {
        const condition = blocks.every(block => block.getAttribute('data-menu-block') !== 'large')
        const left = condition ? -blocks[0].getBoundingClientRect().width / 2 : 0
        TweenMax.set(content, { left })
      }
    })
  }

  setEvents() {
    this.$els.voices.forEach(el => {
      this.$ev.on(el, 'mouseenter', this.mouseenter.bind(this, el))
      this.$ev.on(qs('.c-nav__voice', el), 'click', this.click.bind(this, el))
    })

    this.$ev.on(this.$els.backLayer, 'click', this.closeMenu.bind(this))
    this.$ev.on(this.$el, 'keyup', e => {
      if (e.keyCode === 27) {
        this.closeMenu()
      }
    })
  }

  mouseenter(el) {
    const { width, left } = el.getBoundingClientRect()
    TweenMax.to(this.$els.hoverSlider, 0.5, {
      width,
      x: left - this.hoverSliderLeft,
      ease: Power2.easeInOut
    })
  }

  click(el, e) {
    if (this.getState('isAnimating')) return
    this.setState('isAnimating', true)

    e.stopPropagation()
    e.preventDefault()

    const oldIndex = this.getState('currentIndex')
    this.setState('currentIndex', this.$els.voices.indexOf(el))
    const currentIndex = this.getState('currentIndex')

    const $oldMenu = qsa('[data-menu]')[oldIndex]
    const $menu = qsa('[data-menu]')[currentIndex]
    this.currentMenu = $menu

    if (this.getState('isOpen')) {
      if (currentIndex === oldIndex) {
        this.closeMenu()
      } else {
        this.checkMouseMove()
        this.switchMenu($oldMenu, $menu)
      }
    } else {
      this.checkMouseMove()
      this.openMenu($menu)
    }
  }

  getBlocks(ctx) { return qsa('[data-menu-block]', ctx) }
  getBanners(ctx) { return qsa('[data-menu-banner]', ctx) }
  getContainer(ctx) { return qsa('[data-menu-container]', ctx) }

  getExitTimeline(el, invisible) {
    return new Promise(resolve => {
      const duration = invisible ? 0 : 1
      const tl = new TimelineMax({ onComplete: resolve })

      tl.add('leave')

      if (!invisible) {
        tl.to(this.$els.closeButton, 0.5, { autoAlpha: 0 }, 'leave')
      }

      if (this.$refs.languages.getState('isOpen')) {
        this.$refs.languages.triggerMenu()
      }

      tl
        .set(this.$els.backLayer, { className: '-=is-active' })
        .staggerTo(this.getBlocks(el), 0.5, {
          autoAlpha: 0
        }, 0.1, 'leave')
        .staggerFromTo(this.getBanners(el), 0.5, {
          autoAlpha: 1,
          yPercent: 0
        }, {
          autoAlpha: 0,
          ease: Power2.easeInOut,
          yPercent: 5
        }, 0.1, 'leave')
        .to(qsa('[data-hide]', el), 0.5, {
          autoAlpha: 0
        }, 0.1, 'leave')
        .to(el, duration, {
          scaleY: 0,
          clearProps: 'all',
          ease: Expo.easeInOut
        })
        .set(el, { className: '-=is-active' })
    })
  }

  getEnterTimeline(el, invisible) {
    return new Promise(resolve => {
      const duration = invisible ? 0 : 1.2
      const tl = new TimelineMax({ onComplete: resolve })

      tl
        .set(this.getBlocks(el), { autoAlpha: 0 })
        .set(this.getBanners(el), { autoAlpha: 0 })
        .set(this.$els.backLayer, { className: '+=is-active' })
        .fromTo(el, duration, {
          scaleY: 0
        }, {
          scaleY: 1,
          ease: Expo.easeInOut,
          className: '+=is-active'
        })
        .add('enter')
        .fromTo(qsa('[data-hide]', el), 0.5, {
          autoAlpha: 0
        }, {
          autoAlpha: 1,
          clearProps: 'all'
        }, 'enter')
        .to(this.$els.currentContainer, 0.5, {
          x: 0,
          pointerEvents: 'none',
          clearProps: 'transform, pointerEvents'
        }, 'enter')
        .staggerFromTo(this.getBlocks(el), 0.7, {
          autoAlpha: 0
        }, {
          autoAlpha: 1,
          clearProps: 'all'
        }, 0.1, 'enter')
        .staggerFromTo(this.getBanners(el), 0.7, {
          autoAlpha: 0,
          yPercent: 5
        }, {
          autoAlpha: 1,
          ease: Power2.easeInOut,
          yPercent: 0,
          clearProps: 'all'
        }, 0.1, 'enter')

      if (!invisible) {
        tl.fromTo(this.$els.closeButton, 1.6, {
          autoAlpha: 0,
          scale: 1.2
        }, {
          autoAlpha: 1,
          scale: 1,
          ease: Expo.easeInOut,
          clearProps: 'all'
        }, 'enter+=0.4')
      }
    })
  }

  openMenu(el) {
    if (hasClass(document.body, 'is-header-light')) toggleClass(document.body, 'is-header-light')

    this.setState('isOpen', true)
    this.getEnterTimeline(el, false).then(() => this.setState('isAnimating', false))
  }

  closeMenu() {
    const el = qsa('[data-menu]')[this.getState('currentIndex')]
    this.getExitTimeline(el, false).then(() => {
      this.setState('isAnimating', false)
      this.setState('isOpen', false)
    })
  }

  switchMenu(old, el) {
    this.getExitTimeline(old, true)
    .then(() => this.getEnterTimeline(el, true))
    .then(() => this.setState('isAnimating', false))
  }

  checkMouseMove() {
    if (this.mouseMoveHandler) {
      this.mouseMoveHandler.destroy()
    }

    this.$els.currentMenu = this.$els.menus[this.getState('currentIndex')]
    this.$els.currentContainer = qs('[data-menu-container]', this.$els.currentMenu)

    const bounds = this.$els.currentMenu.getBoundingClientRect()
    this.menuLeft = bounds.left
    this.menuWidth = bounds.width

    if (this.$els.currentContainer.clientWidth > this.$els.currentMenu.clientWidth) {
      this.mouseMoveHandler = handleEvent('mousemove', {
        onElement: this.$els.currentMenu,
        withCallback: this.mouseMove.bind(this)
      })
    }
  }

  mouseMove(event) {
    const mouse = (event.clientX - (this.menuWidth / 2)) - this.menuLeft
    const delta = this.$els.currentContainer.clientWidth - this.menuWidth
    const x = -(mouse * delta) / this.menuWidth
    TweenMax.to(this.$els.currentContainer, 0.5, { x })
  }

  menuScroll({ direction, oldDirection }) {
    // menu directions
    if (this.$el && !this.getState('isOpen')) {
      if (
        hasClass(document.body, 'is-home') ||
        hasClass(document.body, 'is-collection') ||
        hasClass(document.body, 'is-mood') ||
        hasClass(document.body, 'is-adcampaign')
      ) {
        if (window.scrollY > 600 && hasClass(document.body, 'is-header-light')) {
          toggleClass(document.body, 'is-header-light')
        } else if (window.scrollY === 0 && !hasClass(document.body, 'is-header-light')) {
          addClass(document.body, 'is-header-light')
        }
      }

      if (direction === 'down' && !hasClass(document.body, 'is-header-hidden')) {
        addClass(document.body, 'is-header-hidden')
      }

      if (direction !== oldDirection && hasClass(document.body, 'is-header-hidden')) {
        toggleClass(document.body, 'is-header-hidden')
      }
    }
  }
}

export default Menu