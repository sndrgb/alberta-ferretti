import { TimelineMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'

import RafScroll from './scrollbar/scroll'
import { qsa, isMobile, hasClass, addClass, removeClass } from '../base/utils'

class NeedHelp extends Component {

  mounted() {
    this.$els.toggles = qsa('[data-toggle-help]', this.container)

    this.on('change:isOpen', this.toggleNeedHelp.bind(this))

    if (isMobile()) {
      RafScroll.add(this.scrollHandle.bind(this))
    }

    this.$els.toggles.forEach(el => {
      this.$ev.on(el, 'click', () => {
        if (this.getState('isAnimating')) return
        this.setState('isAnimating', true)
        this.setState('isOpen', !this.getState('isOpen'))
      })
    })
  }

  setInitialState() {
    return {
      isOpen: false
    }
  }

  toggleNeedHelp(val) {
    const tl = new TimelineMax({
      onComplete: () => {
        this.setState('isAnimating', false)
      }
    })
    const toggleBounds = this.$els.toggles[0].getBoundingClientRect()

    if (val) {
      tl
      .set('#need-help-container', { autoAlpha: 0 })
      .set('#need-help', { autoAlpha: 1 })
      .add('start')
      .to(this.$els.toggles[0], 0.4, {
        autoAlpha: 0
      }, 'start+=0.3')
      .from('#need-help', 0.7, {
        width: toggleBounds.width,
        height: toggleBounds.height,
        ease: Expo.easeInOut
      }, 'start')
      .to('#need-help-container', 0.4, { autoAlpha: 1 })
    } else {
      tl
      .to('#need-help-container', 0.4, { autoAlpha: 0 })
      .add('start')
      .to(this.$els.toggles[0], 0.5, {
        autoAlpha: 1
      }, 'start')
      .to('#need-help', 0.5, {
        width: toggleBounds.width,
        height: toggleBounds.height,
        clearProps: 'all'
      }, 'start')
    }
  }

  scrollHandle() {
    // menu directions
    hasClass(this.$el, 'is-visible')
    if (!hasClass(this.$el, 'is-visible') && window.scrollY > 100) {
      addClass(this.$el, 'is-visible')
    } else if (window.scrollY <= 100) {
      removeClass(this.$el, 'is-visible')
    }
  }
}

export default NeedHelp