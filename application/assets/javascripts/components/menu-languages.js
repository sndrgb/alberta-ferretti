import { TimelineMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'

import { qsa, qs, addClass, toggleClass } from '../base/utils'

class MenuLanguages extends Component {

  afterInit() {
    this.$els.triggers = qsa('[data-trigger-countries]', this.$el)
    // this.$els.shipping = qs('#languages-shipping', this.$el)
    this.$els.wrapper = qs('#languages-wrapper', this.$el)
    this.$els.languages = qs('#languages-languages', this.$el)
    this.$els.countries = qs('#languages-countries', this.$el)

    this.triggerMenu = this.triggerMenu.bind(this)
    this.setEvents()
    addClass(qs('[data-trigger-countries="open"]', this.$el), 'is-visible')

    this.on('change:isOpen', val => {
      if (val) {
        this.open()
      } else {
        this.close()
      }
    })
  }

  setInitialState() {
    return {
      isOpen: false
    }
  }

  setEvents() {
    this.$els.triggers.forEach(el => {
      this.$ev.on(el, 'click', this.triggerMenu.bind(this))
    })
  }

  triggerMenu() {
    this.$els.triggers.forEach(el => toggleClass(el, 'is-visible'))
    this.setState('isOpen', !this.getState('isOpen'))
  }

  open() {
    const tl = new TimelineMax()
    const wrapperBounds = this.$els.wrapper.getBoundingClientRect()
    const countriesBounds = this.$els.countries.getBoundingClientRect()
    const delta = (qs('.c-nav-wrapper').clientHeight - countriesBounds.height) / 2
    const y = -((wrapperBounds.height + wrapperBounds.top) - delta)

    tl
      .add('start')
      .to(this.$el, 0.8, { y, ease: Expo.easeInOut }, 'start')
      .to(this.$els.countries, 0.8, {
        autoAlpha: 1
      }, 'start')
      .to(this.$els.wrapper, 0.8, {
        autoAlpha: 0
      }, 'start')
  }

  close() {
    const tl = new TimelineMax()

    tl
      .add('start')
      .to(this.$el, 0.8, { y: 0, ease: Expo.easeInOut }, 'start')
      .to(this.$els.countries, 0.8, {
        autoAlpha: 0,
        clearProps: 'all'
      }, 'start')
      .to(this.$els.wrapper, 0.8, {
        autoAlpha: 1,
        clearProps: 'all'
      }, 'start')
  }

  beforeDestroy() {
    if (this.getState('isOpen')) {
      this.triggerMenu()
    }
  }
}

export default MenuLanguages