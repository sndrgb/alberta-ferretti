import { Expo, TimelineMax, TweenMax } from 'gsap/all'

import Modal from './modal'
import { qs, qsa, removeClass, addClass, setBodyFixed } from '../base/utils'

export default class Panel extends Modal {

  mounted() {
    super.mounted()

    // this.id = this.$el.getAttribute('data-panel')
    this.direction = this.$el.getAttribute('data-direction')
    this.$els.wrappers = qsa('[data-panel-wrapper]', this.$el)
    this.$els.toggles = []

    this.$els.wrappers.forEach(el => {
      const attr = el.getAttribute('data-panel-wrapper')
      this.$els.toggles.push(...qsa(`[data-panel-toggle="${attr}"]`))
    })

    this.$els.toggles.push(qs('[data-panel-toggle]', this.$el))

    if (this.$els.wrappers) {
      TweenMax.set(this.$els.wrappers, { autoAlpha: 0, display: 'none' })
    }

    let xPercent = 0
    let yPercent = 0
    if (this.direction === 'left') {
      xPercent = -100
    } else if (this.direction === 'right') {
      xPercent = 100
    } else if (this.direction === 'top') {
      yPercent = 100
    } else {
      yPercent = -100
    }

    this.animation = {
      xPercent,
      yPercent
    }

    this.setEvents()

    this.on('change:activePanel', (val) => {
      if (!val) return
      this.$els.currentPanel = this.$els.wrappers.filter(el => el.getAttribute('data-panel-wrapper') === val)
    })
  }

  setEvents() {
    this.$els.toggles.forEach(el => {
      this.$ev.on(el, 'click', (e) => {
        e.preventDefault()
        e.stopPropagation()
        if (this.getState('isAnimating')) return
        this.setState('isAnimating', true)

        this.setState('activePanel', el.getAttribute('data-panel-toggle'))
        this.setState('isModalOpen', !this.getState('isModalOpen'))
      })
    })
  }

  show() {
    super.show()

    addClass(document.body, 'is-overlay')
    setBodyFixed(true)

    const { xPercent, yPercent } = this.animation
    const tl = new TimelineMax({
      onComplete: () => {
        this.setState('isAnimating', false)
        this.setState('isModalOpen', true)
      }
    })

    tl
    .set(this.$els.currentPanel, { autoAlpha: 1, display: 'block' })
    .fromTo(this.$el, 1.4, {
      xPercent,
      yPercent,
      autoAlpha: 1
    }, {
      xPercent: 0,
      yPercent: 0,
      pointerEvents: 'all',
      clearProps: 'transform',
      ease: Expo.easeInOut
    })
  }

  getDefaultOptions() {
    return {
      isChild: false
    }
  }

  hide() {
    super.hide()

    if (!this.options.isChild) {
      removeClass(document.body, 'is-overlay')
      setBodyFixed(false)
    }

    const { xPercent, yPercent } = this.animation
    const tl = new TimelineMax({
      onComplete: () => {
        this.setState('isAnimating', false)
        this.setState('isModalOpen', false)
        this.$els.toggles.forEach(el => removeClass(el, 'is-active'))
      }
    })

    this.setState('isAnimating', true)

    tl
    .to(this.$el, 1.4, {
      xPercent,
      yPercent,
      clearProps: 'transform',
      pointerEvents: 'none',
      ease: Expo.easeInOut
    })
    .set(this.$el, { autoAlpha: 0 })
    .set(this.$els.wrappers, { autoAlpha: 0, display: 'none' })
    .add(() => {
      this.$el.scrollTop = 0
    })
  }

  getInitialState() {
    return {
      activePanel: false
    }
  }
}