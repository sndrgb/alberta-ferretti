import { Component } from 'yuzu'
import Slideer from './slideshows/slideer'
import Animations from './slideshows/animations'
import Initters from './slideshows/initters'
import { isMobile } from '../base/utils'

class MultiCarousel extends Component {

  mounted() {
    const slides = Array.from(this.$el.querySelectorAll('[data-slide]'))
    const callback = Animations.get('multi')
    const count = isMobile() ? slides.length : slides.length - 2
    const slider = new Slideer(this.$el, {
      length: count,
      loop: true,
      dots: false,
      callback: (event) => callback(event, slider, slides)
    }).init()

    Initters.get('multi')(slider, slides, 0)
  }
}

export default MultiCarousel