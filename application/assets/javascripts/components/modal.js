import { Component } from 'yuzu'

import { appendBackLayer, removeClass, addClass, setBodyFixed } from '../base/utils'

class Modal extends Component {

  mounted() {
    this._restrictedEvents = [
      'focus',
      'DOMMouseScroll',
      'mousewheel',
      'touchmove',
      'scroll'
    ]

    this.restrictFocus = function restrictFocus(e) {
      if (this.getState('isModalOpen') && !this.el.contains(e.target)) {
        e.stopPropagation()
        this.el.focus()
      }
    }.bind(this)

    this.$el.setAttribute('role', 'dialog')
    this.$el.setAttribute('tabindex', '-1')
    this.$el.setAttribute('aria-hidden', 'false')

    this.on('change:isModalOpen', (val, oldVal) => {
      if (val === oldVal) return

      if (val) {
        this.show()
      } else {
        this.hide()
      }
    })
  }

  getDefaultOptions() {
    return { isChild: false }
  }

  getInitialState() {
    return { isModalOpen: false }
  }

  show() {
    this.$el.setAttribute('tabindex', '0')
    this.$el.setAttribute('aria-hidden', 'false')
    this.$el.focus()

    addClass(document.body, 'is-overlay')
    setBodyFixed(true)

    this.backHandler = appendBackLayer(document.body, () => { this.setState('isModalOpen', false) })
    this.keyHandler = this.$ev.on(this.$el, 'keyup', e => {
      if (e.keyCode === 27) {
        this.setState('isModalOpen', false)
      }
    })

    this._restrictedEvents.forEach((event) => {
      document.addEventListener(event, this.restrictFocus, true)
    })
  }

  hide() {
    this.$el.setAttribute('tabindex', '-1')
    this.$el.setAttribute('aria-hidden', 'true')

    if (this.backHandler) this.backHandler()
    if (this.keyHandler) this.keyHandler()


    if (!this.options.isChild) {
      removeClass(document.body, 'is-overlay')
      setBodyFixed(false)
    }

    this._restrictedEvents.forEach((event) => {
      document.removeEventListener(event, this.restrictFocus, true)
    })
  }
}

export default Modal