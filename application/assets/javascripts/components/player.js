import { TimelineMax, Expo } from 'gsap/all'
import Plyr from 'plyr'

import { Component } from 'yuzu'
import { qs, handleEvent, addClass, removeClass, stringToDOM, setBodyFixed } from '../base/utils'

class Player extends Component {

  mounted() {
    this.$els.button = qs('[data-play]', this.$el)

    this.$els.panel = qs('[data-panels="video"]')
    this.$els.innerPanel = qs('[data-panels-inner]', this.$els.panel)
    this.$els.closeButton = qs('[data-close="video"]', this.$els.panel)

    this.setEvents()

    this.on('change:isOpen', val => {
      if (val) {
        setBodyFixed(true)
        addClass(document.body, 'is-overlay')
        this.appendVideo()
      } else {
        setBodyFixed(false)
        removeClass(document.body, 'is-overlay')
        removeClass(this.$els.panel, 'is-open')
        this.removeVideo()
      }
    })
  }

  setEvents() {
    handleEvent('click', {
      onElement: this.$els.button,
      withCallback: (e) => this.click(e)
    })
  }

  click(e) {
    if (this.getState('isAnimating')) return
    e.preventDefault()
    e.stopPropagation()

    this.setState('isAnimating', true)

    if (this.getState('isOpen')) {
      this.close()
    } else {
      this.open()
    }
  }

  appendVideo() {
    const type = this.$el.getAttribute('data-video-provider')
    const id = this.$el.getAttribute('data-video-id')
    this.$els.player = stringToDOM(`<div class="c-video-player" data-plyr-provider="${type}" data-plyr-embed-id="${id}"></div>`)
    this.$els.innerPanel.appendChild(this.$els.player)

    this.player = new Plyr(this.$els.player, {
      controls: ['progress', 'volume']
    })

    this.closeHandle = this.$ev.on(this.$els.closeButton, 'click', this.click.bind(this))
  }

  removeVideo() {
    if (this.player) {
      this.closeHandle()
      this.player.destroy()
      this.$els.innerPanel.innerHTML = ''
    }
  }

  open() {
    const tl = new TimelineMax()
    this.setState('isOpen', true)

    tl
      .set(this.$els.panel, {
        className: '+=is-open'
      })
      .fromTo(this.$els.panel, 1.5, {
        autoAlpha: 0,
        scale: 1.1
      }, {
        autoAlpha: 1,
        scale: 1,
        ease: Expo.easeInOut,
        onComplete: () => {
          this.setState('isAnimating', false)
          console.log(this.player)
          this.player.play()
        }
      })
  }

  close() {
    const tl = new TimelineMax()

    tl
      .fromTo(this.$els.panel, 1.5, {
        autoAlpha: 1,
        scale: 1
      }, {
        autoAlpha: 0,
        scale: 0.9,
        ease: Expo.easeInOut,
        onComplete: () => {
          this.setState('isOpen', false)
          this.setState('isAnimating', false)
        }
      })
      .set(this.$els.panel, {
        className: '-=is-open'
      })
  }

  getInitialState() {
    return {
      isOpen: false,
      isAnimating: false
    }
  }
}

export default Player