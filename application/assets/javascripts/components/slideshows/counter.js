import { Component } from 'yuzu'

import { qs } from '../../base/utils'

class Counter extends Component {

  mounted() {
    this.$els.count = qs('[data-counter="count"]', this.$el)
    this.$els.total = qs('[data-counter="total"]', this.$el)
  }

  bindStateEvents() {
    return {
      count(text) {
        this.$els.count.innerHTML = text
      },

      total(text) {
        this.$els.total.innerHTML = text
      }
    }
  }
}

export default Counter