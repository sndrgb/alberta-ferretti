import { Power4, TimelineMax, TweenMax, Expo } from 'gsap/all'
import { qsa, qs, isMobile } from '../../base/utils'

const getOpen = () => [0, 0, 0, 100, 100, 100, 100, 0]
const getClose = () => [100, 0, 100, 100, 100, 100, 100, 0]

const Animations = new Map()
const getTl = (me) => new TimelineMax({
  paused: true,
  onComplete: () => {
    me.animating = false
  }
})

Animations.set('split', (event, slider, slides) => {
  const me = slider
  const index = event.current // array index
  const previous = event.previous // array index
  const tl = getTl(me)
  const images = qsa('[data-image]', me.el)
  const currentImages = qsa('[data-enter]', images[index])
  const currentTexts = qsa('[data-enter]', slides[index])
  const oldTexts = qsa('[data-enter]', slides[previous])
  const from = getClose()
  const to = getOpen()

  me.animating = true

  tl
    .add('start')
    .set(slides[previous], { className: '-=is-active', clearProps: 'all' })
    .set(slides[index], { autoAlpha: 1, className: '+=is-active', clearProps: 'all' })
    .to(oldTexts, 1, {
      autoAlpha: 0,
      ease: Power4.easeInOut,
      clearProps: 'all'
    }, 'start')
    .staggerFromTo(currentTexts, 1.4, {
      autoAlpha: 0,
      x: 10
    }, {
      autoAlpha: 1,
      x: 0,
      ease: Power4.easeInOut
    }, 0.1, 'start')
    .set(images, { zIndex: -1, className: '-=is-active' }, 'start')
    .set(images[index], { className: '+=is-active', autoAlpha: 1, zIndex: 1 }, 'start')
    .set(images[previous], { autoAlpha: 1, zIndex: 0 }, 'start')
    .to(from, 1.8, to, 'start')
    .set(images, { clearProps: 'all' })

  to.onUpdate = () => {
    TweenMax.set(currentImages, {
      clipPath: `
        polygon(
          ${from[0]}% ${from[1]}%,
          ${from[2]}% ${from[3]}%,
          ${from[4]}% ${from[5]}%,
          ${from[6]}% ${from[7]}%
        )`,
      '-webkit-clip-path': `
        polygon(
          ${from[0]}% ${from[1]}%,
          ${from[2]}% ${from[3]}%,
          ${from[4]}% ${from[5]}%,
          ${from[6]}% ${from[7]}%
        )`
    })
  }

  to.ease = Expo.easeInOut

  tl.restart()
})

Animations.set('quick', (event, slider, slides) => {
  const me = slider
  const index = event.current // array index
  const previous = event.previous // array index
  const tl = getTl(me)
  const currentTexts = qsa('[data-enter]', slides[index])
  const oldTexts = qsa('[data-enter]', slides[previous])
  const duration = isMobile() ? 1 : 1.5

  me.animating = true

  tl
    .add('start')
    .to(oldTexts, 0.5, { autoAlpha: 0 })
    .to(slides, duration, {
      x: -slides[0].clientWidth * index,
      ease: Expo.easeInOut
    }, 0, 0)
    .staggerFromTo(currentTexts, 0.5, {
      autoAlpha: 0,
      y: 5
    }, {
      autoAlpha: 1,
      y: 0,
      clearProps: 'all'
    }, 0.1, '-=0.5')

  tl.restart()
})


Animations.set('fade', (event, slider, slides) => {
  const me = slider
  const dir = event.direction
  const index = event.current // array index
  const previous = event.previous // array index
  const tl = getTl(me)

  me.animating = true

  tl
    .set(slides[previous], { zIndex: 3 })
    .set(slides[index], { zIndex: 4, autoAlpha: 0, xPercent: 20 * dir })
    .add('start')
    .to(slides[index], 0.5, {
      autoAlpha: 1,
      xPercent: 0
    }, 'start')
    .to(slides[previous], 0.5, {
      autoAlpha: 0,
      xPercent: -20 * dir
    }, 'start')
    .set(slides[previous], { zIndex: 1, clearProps: 'all' })

  tl.restart()
})


Animations.set('multi', (event, slider, slides) => {
  const me = slider
  const index = event.current
  const tl = getTl(me)
  const bounds = slides[index].getBoundingClientRect()

  me.animating = true

  tl
    .add('start')
    .to(slides, 1.2, {
      x: -bounds.width * index,
      ease: Expo.easeInOut
    }, 'start')

  tl.restart()
})

Animations.set('fullscreen', (event, slider, slides) => {
  const me = slider
  const index = event.current // array index
  const previous = event.previous // array index
  const tl = getTl(me)
  const currentImages = qsa('[data-enter="image"]', slides[index])
  const currentTexts = qsa('[data-enter="text"]', slides[index])
  const oldTexts = qsa('[data-enter="text"]', slides[previous])

  let from = getClose()
  let to = getOpen()

  me.animating = true

  tl
    .set(slides[index], { autoAlpha: 0, zIndex: 2, className: '+=is-active' })
    .set(slides[previous], { zIndex: 1, className: '-=is-active' })
    .to(oldTexts, 1, {
      autoAlpha: 0,
      ease: Power4.easeInOut
    })
    .add('start')
    .set(slides[index], { autoAlpha: 1, zIndex: 2 }, 'start')
    .staggerFromTo(currentTexts, 1.4, {
      autoAlpha: 0,
      x: 10
    }, {
      autoAlpha: 1,
      x: 0,
      ease: Power4.easeInOut
    }, 0.1, 'start')
    .to(from, 1.8, to, 'start')
    .set(slides[previous], { clearProps: 'all' })

  to.onUpdate = () => {
    TweenMax.set(currentImages, {
      clipPath: `
        polygon(
          ${from[0]}% ${from[1]}%,
          ${from[2]}% ${from[3]}%,
          ${from[4]}% ${from[5]}%,
          ${from[6]}% ${from[7]}%
        )`,
      '-webkit-clip-path': `
        polygon(
          ${from[0]}% ${from[1]}%,
          ${from[2]}% ${from[3]}%,
          ${from[4]}% ${from[5]}%,
          ${from[6]}% ${from[7]}%
        )`
    })
  }

  to.ease = Expo.easeInOut
  tl.restart()
})

Animations.set('instagram', (event, slider, slides) => {
  const me = slider
  const index = event.current // array index
  me.animating = true

  const tl = new TimelineMax({
    paused: true,
    onComplete: () => {
      me.animating = false
    }
  })

  tl.to(slides, 1.6, {
    x: -slides[0].clientWidth * index,
    ease: Expo.easeInOut
  })

  tl.restart()
})


export default Animations