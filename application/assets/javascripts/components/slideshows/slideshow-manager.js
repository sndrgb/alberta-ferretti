import { Component } from 'yuzu'

import Slideer from './slideer'
import Animations from './animations'
import Initters from './initters'

class SlideshowManager extends Component {

  mounted() {
    this.generateSlideShow()
  }

  generateSlideShow() {
    const slides = Array.from(this.$el.querySelectorAll('[data-slide]'))
    const callback = Animations.get(this.options.type)
    const dots = this.options.dots !== undefined ? this.options.dots : true

    this.slider = new Slideer(this.$el, {
      length: slides.length,
      loop: true,
      dots,
      callback: (event) => callback(event, this.slider, slides)
    }).init()

    Initters.get(this.options.type)(this.slider, slides, 0)
  }

  removeSlideshow() {
    this.slider.destroy()
  }
}

export default SlideshowManager