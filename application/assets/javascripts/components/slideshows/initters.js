import { TweenMax } from 'gsap/all'
import { qsa } from '../../base/utils'

const Initters = new Map()

Initters.set('split', (slider, slides, currentSlideIdx) => {
  const currentSlide = slides[currentSlideIdx]
  const images = qsa('[data-image]', slider.el)
  const currentEnters = qsa('[data-enter]', currentSlide)
  const currentImage = qsa('[data-image]', slider.el)[0]

  if (!currentSlide) return

  TweenMax.set([...slides], { position: 'relative' })
  const height = slides.reduce((prev, current) => (prev.clientHeight > current.clientHeight) ? prev.clientHeight : current.clientHeight)
  TweenMax.set(slides[0].parentElement, { height })
  TweenMax.set([...slides], { clearProps: 'all' })

  TweenMax.set([...images, ...slides], { autoAlpha: 0, zIndex: 0 })
  TweenMax.set([...currentEnters, currentImage, currentSlide], { autoAlpha: 1, zIndex: 2 })
})

Initters.set('quick', (slider, slides, currentSlideIdx) => {
  const currentSlide = slides[currentSlideIdx]

  if (!currentSlide) return

  const oldTexts = qsa('[data-enter]', slider.el)
  const currentTexts = qsa('[data-enter]', slides[currentSlideIdx])

  TweenMax.set(oldTexts, { autoAlpha: 0 })
  TweenMax.set(currentTexts, { autoAlpha: 1 })
})

Initters.set('fade', (slider, slides, currentSlideIdx) => {
  const currentSlide = slides[currentSlideIdx]

  if (!currentSlide) return

  TweenMax.set(currentSlide, { autoAlpha: 1 })
})

Initters.set('multi', (slider, slides, currentSlideIdx) => {
  const currentSlide = slides[currentSlideIdx]

  if (!currentSlide) return

  TweenMax.set(currentSlide, { autoAlpha: 1 })
})

Initters.set('fullscreen', (slider, slides, currentSlideIdx) => {
  const currentSlide = slides[currentSlideIdx]
  const images = qsa('[data-image]', slider.el)
  const currentEnters = qsa('[data-enter]', currentSlide)
  const currentImage = qsa('[data-image]', slider.el)[0]

  if (!currentSlide) return

  TweenMax.set([...images, ...slides], { autoAlpha: 0, zIndex: 0 })
  TweenMax.set([...currentEnters, currentImage, currentSlide], { autoAlpha: 1, zIndex: 2 })
})

Initters.set('instagram', () => {
  return false
})

export default Initters