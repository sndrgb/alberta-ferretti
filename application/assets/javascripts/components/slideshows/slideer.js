import Hammer from 'hammerjs'
import { Component } from 'yuzu'

import { qs, stringToDOM } from '../../base/utils'
import Counter from './counter'
import Dots from './dots'

class Slideer extends Component {

  mounted() {
    if (!this.$el) {
      console.error('You need to provide an element in costructor')
    }

    if (!this.options.callback) {
      console.error('You need to provide a callback function in the options')
    }

    this.animating = false

    this.index = this.options.startIndex ? this.options.startIndex : 0
    this.length = this.options.length - 1

    this.hammer = null
    this.onSwipe = this.onSwipe.bind(this)
  }

  getInitialState() {
    return {
      currentCount: 1,
      currentIndex: this.index,
      totalCount: this.options.length
    }
  }

  getDefaultOptions() {
    return {
      loop: false,
      delta: 1,
      startIndex: 0,
      nextSelector: '[data-next]',
      prevSelector: '[data-prev]'
    }
  }

  afterInit() {
    this.setEvents()

    if (this.options.counter) {
      this.setCounter()
    }

    if (this.options.dots) {
      this.setDots()
    }
  }

  setEvents() {
    this.hammer = new Hammer.Manager(this.$el)
    this.hammer.add(new Hammer.Swipe({
      direction: Hammer.DIRECTION_HORIZONTAL
    }))
    this.hammer.on('swipe', this.onSwipe)

    this.$els.prevControl = this.$el.querySelector(this.options.prevSelector)
    this.$els.nextControl = this.$el.querySelector(this.options.nextSelector)

    this.nextHandler = () => { this.goTo.call(this, this.getCurrentSlide() + 1) }
    this.prevHandler = () => { this.goTo.call(this, this.getCurrentSlide() - 1) }

    if (this.$els.prevControl && this.$els.nextControl) {
      this.$els.prevControl.addEventListener('click', this.prevHandler)
      this.$els.nextControl.addEventListener('click', this.nextHandler)
    }
  }

  setCounter() {
    this.$els.counter = stringToDOM(`
      <div class="c-counter">
        <span data-counter="count">${this.getState('currentCount')}</span>
        <span>/</span>
        <span data-counter="total">${this.getState('totalCount')}</span>
      </div>
    `)

    this.$el.appendChild(this.$els.counter)
    this.setRef({
      id: 'counter',
      component: Counter,
      el: this.$els.counter,
      props: {
        currentCount: 'count',
        totalCount: 'total'
      }
    })
  }

  setDots() {
    this.$els.dots = qs('[data-dots]', this.$el)

    if (!this.$els.dots) {
      this.$els.dots = stringToDOM('<div class="c-dots"></div>')
      this.$el.appendChild(this.$els.dots)
    }

    this.setRef({
      id: 'dots',
      component: Dots,
      el: this.$els.dots,
      opts: {
        length: this.options.length
      },
      props: {
        currentIndex: 'index',
        totalCount: 'total'
      }
    })
  }

  beforeDestroy() {
    this.hammer.off('swipe', this.onSwipe)
    this.hammer.destroy()
    this.hammer = null

    if (this.$els.prevControl && this.$els.nextControl) {
      this.$els.prevControl.removeEventListener('click', this.prevHandler)
      this.$els.nextControl.removeEventListener('click', this.nextHandler)
    }
  }

  getNext(delta) {
    const next = delta >= this.options.delta ? this.index - 1 : this.index + 1

    return this.checkLoop(next)
  }

  checkLoop(next) {
    if (next < 0) {
      return this.options.loop ? this.length : 0
    }

    if (next > this.length) {
      return this.options.loop ? 0 : this.length
    }

    return next
  }

  getEvent(index) {
    return {
      current: index,
      previous: this.index,
      direction: index >= this.index ? 1 : -1
    }
  }

  getCurrentSlide() {
    return this.index
  }

  onSwipe(e) {
    const delta = e.deltaX

    if (this.animating || delta > -this.options.delta && delta < this.options.delta) return
    this.animating = true

    this.callback(delta)
  }

  goTo(index) {

    const check = this.checkLoop(index)
    const event = this.getEvent(check)

    if (this.animating) return
    this.animating = true

    this.index = check
    this.options.callback(event)
  }

  callback(delta) {

    const index = this.getNext(delta)
    const event = this.getEvent(index)

    this.setState('currentIndex', index)
    this.setState('currentCount', index + 1)

    this.index = index
    this.options.callback(event)
  }
}

export default Slideer