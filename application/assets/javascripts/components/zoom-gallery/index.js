import { TimelineMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'

import { stringToDOM, qsa, qs, handleEvent, addClass, removeClass, setBodyFixed } from '../../base/utils'
import Zoom from './zoom'

class ZoomGallery extends Component {

  mounted() {
    this.$els.slides = qsa('[data-zoom]', this.$el)
    this.bindEvents()

    // <button class="zoom-button zoom-button--left" data-prev>
    //         <svg class="o-icon o-icon--arrow"><use xlink:href="#arrow-left"/></svg>
    //       </button>
    //       <button class="zoom-button zoom-button--right" data-next>
    //         <svg class="o-icon o-icon--arrow"><use xlink:href="#arrow-right"/></svg>
    //       </button>

    this.$els.zoomContainer = stringToDOM(`
      <div id="zoom-container" style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; zIndex: 20 display: none; pointer-events: none;"></div>
    `)
    document.body.appendChild(this.$els.zoomContainer)

    this.on('change:isZoomOpen', (val) => {
      if (val) {
        setBodyFixed(true)
        addClass(document.body, 'is-overlay')
      }
    })
  }

  setInitialState() {
    return {
      isZoomOpen: false
    }
  }

  bindEvents() {
    this.$els.slides.forEach((el) => {
      handleEvent('click', {
        onElement: el,
        withCallback: (event) => this.clickHandler(event, el)
      })
    })
  }

  clickHandler(event, el) {
    const tl = new TimelineMax()
    this.mouseX = event.clientX
    this.mouseY = event.clientY

    this.$els.lastClicked = el

    this.setState('isZoomOpen', true)
    this.setState('bounds', el.getBoundingClientRect())
    this.appendChild(el)

    tl
      .set(this.$els.zoomContainer, {
        className: '+=is-active',
        display: 'block',
        pointerEvents: 'all'
      })
      .fromTo(this.$els.zoomItem, 1.5, {
        width: this.getState('bounds').width,
        y: this.getState('bounds').y,
        x: this.getState('bounds').x,
        scale: 1.02
      }, {
        width: '100%',
        x: 0,
        y: 0,
        scale: 1,
        ease: Expo.easeInOut
      })
      .add(() => {
        this.$ev.on(this.$els.zoomItem, 'click', this.close.bind(this))

        this.setRef({
          id: 'zoom',
          component: Zoom,
          el: this.$els.zoomContainer,
          props: {
            isZoomOpen: 'isOpen'
          },
          opts: {
            mouseX: event.clientX,
            mouseY: event.clientY
          }
        })
      })
  }

  close() {
    const tl = new TimelineMax({
      onComplete: () => {
        setBodyFixed(false)
        this.$els.zoomContainer.removeChild(this.$els.zoomItem)
      }
    })

    this.setState('isZoomOpen', false)

    tl
      .add('start')
      .add(() => { removeClass(document.body, 'is-overlay') }, 'start+=0.7')
      .to(this.$els.zoomItem, 1.5, {
        width: this.getState('bounds').width,
        y: this.getState('bounds').y,
        x: this.getState('bounds').x,
        ease: Expo.easeInOut
      }, 'start')
      .set(this.$els.zoomContainer, {
        className: '-=is-active',
        display: 'none',
        pointerEvents: 'none'
      })
      .set(qs('img', this.$els.lastClicked), { clearProps: 'all' })
  }

  appendChild(el) {
    const src = el.getAttribute('data-zoom')

    this.$els.zoomItem = stringToDOM(`<img id="zoom-item" src="${src}" />`)
    this.$els.zoomContainer.appendChild(this.$els.zoomItem)
  }
}

export default ZoomGallery