import { TweenMax } from 'gsap/all'
import { Component } from 'yuzu'

import { isMobile, qs } from '../../base/utils'

class Zoom extends Component {

  mounted() {
    this.$els.image = qs('#zoom-item', this.$el)
    this.setState('imageBounds', this.$els.image.getBoundingClientRect())

    this.setState('centerX', this.options.mouseX)
    this.setState('centerY', this.options.mouseY)

    this.excessWidth = this.getState('imageBounds').width - this.$el.clientWidth
    this.excessHeight = this.getState('imageBounds').height - this.$el.clientHeight

    if (!isMobile()) {
      this.$ev.on(this.$el, 'mousemove', this.moveMouse.bind(this))
    }
  }

  bindStateEvents() {
    return {
      isOpen: (val) => {
        if (!val) {
          this.$ev.off(this.$el, 'mousemove')
        }
      }
    }
  }

  moveMouse(event) {
    const mouseoffLeftPage = event.clientX
    const mouseoffTopPage = event.clientY

    const mouseoffLeftPercent = mouseoffLeftPage / this.$el.clientWidth
    const mouseoffTopPercent = mouseoffTopPage / this.$el.clientHeight

    const y = -(mouseoffTopPercent * this.excessHeight)
    const x = -(mouseoffLeftPercent * this.excessWidt)

    TweenMax.to(this.$els.image, 0.5, { x, y })
  }

  moveMobile() {
    if (screen.width < 1000) {
      const imgWidth = this.imgZoomed.clientWidth
      const distanceRemain = (imgWidth - this.imgZoomedCont.clientWidth) / 2
      let count = 0

      this.accelerometerAnim = (event) => {
        const xValue = Math.round(event.gamma)
        const ratio = (Math.abs(xValue) / distanceRemain) * 10
        const incrementPos = 5 * ratio
        const incrementNeg = 3 * ratio

        if (xValue > 5 && count < distanceRemain) {
          count = count += incrementPos
          if (count > distanceRemain) {
            count = distanceRemain
          }
        } else if (xValue < -15 && count > -distanceRemain) {
          count = count -= incrementNeg
          if (count > -distanceRemain && count < -distanceRemain + 5) {
            count = -distanceRemain
          }
        }

        TweenMax.to(this.imgZoomed, 0.5, {
          x: -count,
        })
      }

      if (window.DeviceOrientationEvent) {
        window.addEventListener('deviceorientation', this.accelerometerAnim, true)
      }
    }
  }
}

export default Zoom
