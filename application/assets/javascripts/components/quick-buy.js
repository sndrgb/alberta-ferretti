import { TimelineMax, Expo } from 'gsap/all'

import { qs, qsa, handleEvent, removeClass } from '../base/utils'

import Modal from './modal'
import Slideer from '../components/slideshows/slideer'
import Animations from '../components/slideshows/animations'
import Initters from '../components/slideshows/initters'

class QuickBuy extends Modal {

  mounted() {
    super.mounted()

    this.$els.cta = qsa('[data-toggle]', this.$el)
    this.$els.parentSlides = qsa('[data-slide]', this.$el)
    this.$els.panel = qs('[data-panels="quickbuy"]')
    this.$els.innerPanel = qs('[data-panels-inner]', this.$els.panel)
    this.$els.closeButton = qs('[data-close="quickbuy"]', this.$els.panel)

    this.closeHandles = []

    this.setEvents()

    if (this.$els.parentSlides.length > 1) {
      this.parentSlideshow = new Slideer(this.$el, {
        length: this.$els.parentSlides.length,
        loop: true,
        nextSelector: '[data-next="quick"]',
        prevSelector: '[data-prev="quick"]',
        callback: (event) => Animations.get('quick')(event, this.parentSlideshow, this.$els.parentSlides)
      }).init()

      Initters.get('quick')(this.parentSlideshow, this.$els.parentSlides, 0)
    }

  }

  getInitialState() {
    return {
      isModalOpen: false,
      isAnimating: false,
      currentIndex: 0
    }
  }

  getQuickBuy(url) {
    return new Promise((resolve) => {
      fetch(url).then(v => {
        const data = v.text()
        resolve(data)
      }).catch()
    })
  }

  setEvents() {
    this.clickCta = []
    this.$els.cta.forEach((el, i) => {
      const event = handleEvent('click', {
        onElement: el,
        withCallback: () => this.click(i)
      })

      this.clickCta.push(event)
    })
  }

  click(i) {
    if (this.getState('isAnimating')) return
    this.setState('isAnimating', true)

    this.setState('currentIndex', i)
    this.setState('isModalOpen', !this.getState('isModalOpen'))
  }

  show() {
    super.show()
    const url = this.$els.parentSlides[this.getState('currentIndex')].getAttribute('data-url')

    this.getQuickBuy(url).then(data => {
      const tl = new TimelineMax()
      const condition = this.$el.getBoundingClientRect().left > 100
      const xPercent = condition ? 100 : -100
      const to = condition ? 50 : 0

      if (this.parentSlideshow) {
        this.parentSlideshow.destroy()
      }
      this.setState('isModalOpen', true)

      const handleClose = handleEvent('click', {
        onElement: this.$els.closeButton,
        withCallback: () => this.click()
      })

      this.closeHandles.push(handleClose)
      this.$els.innerPanel.innerHTML = data

      const carousel = qs('[data-product]', this.$els.innerPanel)
      const slides = qsa('[data-product-slide]', carousel)
      this.childSlideshow = new Slideer(carousel, {
        length: slides.length,
        loop: true,
        callback: (event) => Animations.get('fade')(event, this.childSlideshow, slides)
      }).init()

      tl
        .add(() => {
          Initters.get('fade')(this.childSlideshow, slides, 0)
        })
        .set(carousel, { autoAlpha: 1, zIndex: 2 })
        .set(this.$els.panel, { className: '+=is-open' })
        .fromTo(this.$els.panel, 1.5, {
          xPercent,
          autoAlpha: 1
        }, {
          xPercent: to,
          autoAlpha: 1,
          ease: Expo.easeInOut,
          onComplete: () => {
            this.setState('isAnimating', false)
          }
        })
    })
  }

  hide() {
    super.hide()
    this.closeHandles.forEach(el => el.destroy())

    const condition = this.$el.getBoundingClientRect().left > 100
    const xPercent = condition ? 100 : -100
    const tl = new TimelineMax({
      onComplete: () => {
        this.childSlideshow.destroy()
        this.$els.innerPanel.innerHTML = null
      }
    })

    if (this.parentSlideshow) {
      this.parentSlideshow.setEvents()
    }
    this.setState('isModalOpen', false)

    tl
      .to(this.$els.panel, 1.2, {
        xPercent,
        clearProps: 'all',
        ease: Expo.easeInOut,
        onComplete: () => {
          this.setState('isAnimating', false)
          removeClass(this.$els.panel, 'is-open')
        }
      })
  }

  beforeDestroy() {
    this.clickCta.forEach(el => el.destroy())
    this.closeHandles.forEach(el => el.destroy())

    if (this.getState('isModalOpen')) {
      this.setState('isModalOpen', false)
    }
  }
}

export default QuickBuy