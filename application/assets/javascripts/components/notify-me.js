import { TimelineMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'
import { qsa } from '../base/utils'

class Notify extends Component {

  afterInit() {
    this.$els.notify = qsa('[data-notify]')

    this.$els.notify.forEach(el => {
      this.$ev.on(el, 'click', this.toggleNotify.bind(this, el))
    })
  }

  toggleNotify(el) {
    const tl = new TimelineMax()
    const toggle = el.getAttribute('data-notify')

    if (toggle === 'open') {
      tl
        .to('.c-product-variables', 1, { autoAlpha: 0, yPercent: 10, ease: Expo.easeInOut })
        .set('.c-product-variables', { position: 'absolute' })
        .to('.c-product-notifyme', 1, {
          autoAlpha: 1,
          position: 'relative',
          yPercent: 0,
          ease: Expo.easeInOut
        })
    } else {
      tl
      .to('.c-product-notifyme', 1, { autoAlpha: 0, yPercent: 10, ease: Expo.easeInOut })
      .set('.c-product-notifyme', { position: 'absolute', clearProps: 'all' })
      .set('.c-product-variables', { yPercent: 0 })
      .to('.c-product-variables', 1, { autoAlpha: 1, position: 'relative', yPercent: 0, ease: Expo.easeInOut })
    }
  }

}

export default Notify