import { Component } from 'yuzu'

import { qs, addClass } from '../base/utils'

class NewsLetterSubscribe extends Component {

  mounted() {
    this.$els.formTrigger = qs('#newsletter-trigger', this.$el)
    this.$els.successMessage = qs('[data-success]', this.$el)
    this.$els.input = qs('#newsletter-input', this.$els.formTrigger)

    this.subscribeToNewsletter()


    this.on('change:isSuccessful', val => {
      if (val) {
        addClass(this.$el, 'is-success')
      }
    })

    this.on('change:isError', val => {
      if (val) {
        addClass(this.$el, 'is-errror')
      }
    })
  }

  subscribeToNewsletter() {
    if (this.$els.formTrigger) {
      this.$ev.on(this.$els.formTrigger, 'submit', this.triggerByForm.bind(this))
    }
  }

  getInitialState() {
    return {
      isSuccessful: false,
      isError: false
    }
  }

  triggerByForm(e) {
    e.preventDefault()
    e.stopPropagation()

    const val = { email: qs('#footer-newsletter', e.target).value }
    const value = JSON.stringify(val)

    fetch('/', {
      // method: 'POST', // or 'PUT'
      // body: value, // data can be `string` or {object}!
      // headers: {
      //   'Content-Type': 'application/json'
      // }
    }).then(() => {
      this.$els.successMessage.innerHTML = this.$els.successMessage.getAttribute('data-success')
      this.setState('isSuccessful', true)
    }).catch(() => {
      this.$els.successMessage.innerHTML = this.$els.successMessage.getAttribute('data-error')
      this.setState('isError', true)
    })
  }
}

export default NewsLetterSubscribe