import { Component } from 'yuzu'
import Dropdown from './dropdown'
import { qsa } from '../base/utils'

class DropdownController extends Component {
  mounted() {
    this.$els.dropdowns = qsa(this.options.selectors)

    this.$els.dropdowns.forEach((el, i) => {
      this.setRef({
        id: `dropdown${i}`,
        component: Dropdown,
        el,
        opts: {
          $toggle: 'a',
          $menu: 'div'
        }
      })
    })

    Object.values(this.$refs).forEach(listener => {
      listener.on('dropdown:opening', (current) => {
        const others = Object.values(this.$refs).filter(filter => current.el !== filter.el)
        others.forEach(child => child.toggleDropdown(false, true))
      })
    })
  }
}

export default DropdownController