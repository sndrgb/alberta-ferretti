import { Expo, TimelineMax } from 'gsap/all'

import Modal from './modal'
import { qs } from '../base/utils'
import MultiCarousel from './multi-carousel'
import RafScroll from './scrollbar/scroll'

class Instagram extends Modal {

  mounted() {
    super.mounted()

    this.instaScroll = this.instaScroll.bind(this)

    setTimeout(() => {
      RafScroll.add(this.instaScroll)
    }, 2000)

    this.$els.footer = qs('.c-footer')
    this.$els.closeButton = qs('#close-instagram')
    this.setEvents()
  }

  setEvents() {
    this.$ev.on(this.$els.closeButton, 'click', () => { this.setState('isModalOpen', false) })
  }

  hide() {
    super.hide()

    const tl = new TimelineMax()
    this.setState('isModalOpen', false)
    RafScroll.remove(this.instaScroll)
    this.$refs.slideshow.destroy()

    tl
      .to(this.$el, 1.6, {
        yPercent: 100,
        clearProps: 'all',
        ease: Expo.easeInOut
      })
  }

  instaScroll({ direction, scrollY }) {
    const tl = new TimelineMax()
    const totalScroll = (scrollY + window.innerHeight)
    const max = document.body.scrollHeight - this.$els.footer.clientHeight

    if (
      direction === 'down' &&
      totalScroll >= max &&
      !this.getState('isModalOpen')
    ) {
      // RafScroll.remove(this.listener)
      this.setState('isModalOpen', true)
      this.setRef({
        id: 'slideshow',
        component: MultiCarousel,
        el: this.$el
      })

      tl
        .set(this.$el, { autoAlpha: 1 })
        .fromTo(this.$el, 1.6, {
          yPercent: 100
        }, {
          yPercent: 0,
          ease: Expo.easeInOut
        })
    }
  }

  destroy() {
    RafScroll.remove(this.instaScroll)
  }
}

export default Instagram