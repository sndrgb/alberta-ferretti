import { TimelineMax, Expo, TweenMax } from 'gsap/all'
import { Component } from 'yuzu'
import SplitText from '../../../vendors/gsap/commonjs-flat/SplitText'

import Slideer from '../slideshows/slideer'
import { stringToDOM, flatten, qsa, qs, addClass, removeClass, setBodyFixed } from '../../base/utils'

const getOpen = () => [0, 0, 0, 100, 100, 100, 100, 0]
const getClose = () => [100, 0, 100, 100, 100, 100, 100, 0]

class Gallery extends Component {

  mounted() {
    this.$els.images = qsa('[data-gallery]', this.$el)
    this.slides = flatten(this.$els.images.map(el => {
      const data = JSON.parse(el.getAttribute('data-gallery'))

      if (Array.isArray(data)) {
        return data.map(obj => Object.assign({ el }, obj))
      }

      return Object.assign({ el }, data)
    }))

    this.$els.panel = qs('[data-panels="gallery"]')
    this.$els.panelInner = qs('[data-panel-inner]', this.$els.panel)
    this.$els.close = qs('[data-close="gallery"]', this.$els.panel)
    this.$els.prev = qs('[data-prev]', this.$els.panel)
    this.$els.next = qs('[data-next]', this.$els.panel)

    this.$els.prev.appendChild(stringToDOM('<div></div>'))
    this.$els.next.appendChild(stringToDOM('<div></div>'))

    this.slides.forEach((el, i) => {
      this.$els.panelInner.appendChild(stringToDOM(`<div class="c-gallery__slide" data-slide="${i}"></div>`))
      this.$els.slides = qsa('[data-slide]', this.$els.panel)
    })

    this.$els.images.forEach((el, i) => {
      this.$ev.on(el, 'click', (event) => this.open(event, el, i))
    })

    this.on('change:isOpen', (val) => {
      if (val) {
        setBodyFixed(true)
        addClass(document.body, 'is-overlay')
      } else {
        setBodyFixed(false)
        removeClass(document.body, 'is-overlay')
      }
    })
  }

  setInitialState() {
    return {
      isOpen: false
    }
  }

  bindEvents() {
    this.$ev.on(this.$els.close, 'click', (event) => this.close(event))
    //this.$ev.on(this.$els.prev, 'click', this.$refs.slider.goTo.call(this.slider, this.$refs.slider.getCurrentSlide() - 1))
    //this.$ev.on(this.$els.next, 'click', this.$refs.slider.goTo.call(this.slider, this.$refs.slider.getCurrentSlide() + 1))
  }

  open(event, el, i) {
    event.stopPropagation()
    event.preventDefault()

    const tl = new TimelineMax()
    this.setState('isOpen', true)

    this.setRef({
      id: 'slider',
      el: this.$els.panel,
      component: Slideer,
      opts: {
        length: this.slides.length,
        callback: this.sliderCallback.bind(this, event),
        startIndex: i
      }
    })

    // this.$refs.slider.init()
    this.setState('current', this.slides[i])
    this.appendChild()
    this.setupButtons()
    this.bindEvents()

    tl
    .set(this.$els.slides, { autoAlpha: 0 })
    .set(this.$els.slides[i], { autoAlpha: 1 })
    .add('start')
    .fromTo(this.$els.panel, 1.5, {
      autoAlpha: 0
    }, {
      className: '+=is-active',
      autoAlpha: 1,
      ease: Expo.easeInOut
    }, 'start')
    .fromTo(this.$els.panelInner, 1.5, {
      autoAlpha: 0,
      scale: 1.2
    }, {
      scale: 1,
      autoAlpha: 1,
      ease: Expo.easeInOut
    },'start')
  }

  close() {
    const tl = new TimelineMax({
      onComplete: () => {
        this.setState('isOpen', false)
      }
    })

    tl
    .add('start')
    .to(this.$els.panelInner, 1.5, {
      scale: 0.9,
      ease: Expo.easeInOut
    }, 'start')
    .to(this.$els.panel, 1.5, {
      autoAlpha: 0,
      ease: Expo.easeInOut
    }, 'start')
    .set(this.$els.panel, {
      className: '-=is-active',
      clearProps: 'all'
    })
  }

  setupButtons() {
    const currentSlide = this.$refs.slider.getCurrentSlide()
    const tl = new TimelineMax()
    const prevText = this.slides[currentSlide - 1] ? this.slides[currentSlide - 1].title || 'prev' : ''
    const nextText = this.slides[currentSlide + 1] ? this.slides[currentSlide + 1].title || 'next' : ''

    const oldPrevDom = qs('div', this.$els.prev)
    const oldPrevSplits = new SplitText(oldPrevDom, { type: 'chars, words' }).chars
    const oldNextDom = qs('div', this.$els.next)
    const oldNextSplits = new SplitText(oldNextDom, { type: 'chars, words' }).chars

    const newPrevDom = stringToDOM(`<div>${prevText}</div>`)
    this.$els.prev.appendChild(newPrevDom)
    const newPrevSplits = new SplitText(newPrevDom, { type: 'chars, words' }).chars

    const newNextDom = stringToDOM(`<div>${nextText}</div>`)
    this.$els.next.appendChild(newNextDom)
    const newNextSplits = new SplitText(newNextDom, { type: 'chars, words' }).chars

    tl
    .add('start')
    .staggerTo(oldPrevSplits, 0.5, {
      autoAlpha: 0,
      yPercent: 30
    }, 0.02, 'start')
    .staggerTo(oldNextSplits, 0.5, {
      autoAlpha: 0,
      yPercent: 30
    }, 0.02, 'start')
    .staggerFromTo(newPrevSplits, 0.5, {
      autoAlpha: 0,
      yPercent: -30
    }, {
      autoAlpha: 1,
      yPercent: 0
    }, 0.02, 'start')
    .staggerFromTo(newNextSplits, 0.5, {
      autoAlpha: 0,
      yPercent: -30
    }, {
      autoAlpha: 1,
      yPercent: 0
    }, 0.02, 'start')
    .add(() => {
      if (oldPrevDom) this.$els.prev.removeChild(oldPrevDom)
      if (oldNextDom) this.$els.next.removeChild(oldNextDom)
    })
  }

  appendChild() {
    const $currentSlide = this.$els.slides[this.$refs.slider.getCurrentSlide()]

    if (!$currentSlide.children.length) {
      this.$els.item = stringToDOM(`
        <figure>
        <img src="${this.getState('current').image}" />
        <figcaption>${this.getState('current').title || ''}</figcaption>
        </figure>
      `)

      $currentSlide.appendChild(this.$els.item)
    }
  }

  sliderCallback(e, event) {
    const index = event.current
    const prev = event.previous
    const tl = new TimelineMax({
      onComplete: () => {
        this.$refs.slider.animating = false
      }
    })

    this.$refs.slider.animating = true

    this.setState('current', this.slides[index])
    this.setState('prev', this.slides[prev])
    this.setState('next', this.slides[event.next])

    this.appendChild()
    this.setupButtons()

    const newImage = qs('img', this.$els.slides[index])

    const tweenImage = (image) => {
      const to = getOpen()
      const from = getClose()

      to.onUpdate = () => {
        TweenMax.set(image, {
          clipPath: `polygon(
            ${from[0]}% ${from[1]}%,
            ${from[2]}% ${from[3]}%,
            ${from[4]}% ${from[5]}%,
            ${from[6]}% ${from[7]}%
          )`,
          '-webkit-clip-path': `polygon(
            ${from[0]}% ${from[1]}%,
            ${from[2]}% ${from[3]}%,
            ${from[4]}% ${from[5]}%,
            ${from[6]}% ${from[7]}%
          )`
        })
      }

      to.ease = Expo.easeInOut
      return TweenMax.to(from, 1.8, to)
    }

    const oldTitle = qs('figcaption', this.$els.slides[prev])
    const currentTitle = qs('figcaption', this.$els.slides[index])

    tl
    .to(oldTitle, 0.3, { autoAlpha: 0 })
    .set(this.$els.slides[index], { autoAlpha: 1, zIndex: 2 })
    .set(this.$els.slides[index], { autoAlpha: 1, zIndex: 2 })
    .set(this.$els.slides[prev], { zIndex: 1 })
    .add(tweenImage(newImage))
    .fromTo(currentTitle, 0.3, { autoAlpha: 0 }, { autoAlpha: 1 })
    .set(this.$els.slides[prev], { autoAlpha: 0, zIndex: 0 })
  }

  beforeDestroy() {
    this.$els.panelInner.innerHTML = ''
    this.$els.prev.innerHTML = ''
    this.$els.next.innerHTML = ''
  }
}

export default Gallery