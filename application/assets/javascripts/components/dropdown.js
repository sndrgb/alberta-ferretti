import { Component } from 'yuzu'
import { TimelineMax, Expo, TweenMax } from 'gsap/all'
import { qs, qsa, hasClass, removeClass, stringToDOM } from '../base/utils'

class Dropdown extends Component {

  mounted() {
    this.$els.toggle = qs(this.options.$toggle, this.$el) || qs('[data-dropdown-toggle]', this.$el)
    this.$els.menu = qs(this.options.$menu, this.$el) || qs('[data-dropdown-menu]', this.$el)
    this.$els.arrow = qs('.dropdown-arrow', this.$els.toggle)

    // set sub dropdowns
    qsa('[data-dropdown]', this.$el).forEach((el, i) => {
      this.setRef({
        id: `dropdown${i}`,
        component: Dropdown,
        el,
        opts: {
          $toggle: 'a',
          $menu: 'div'
        }
      })
    })

    if (hasClass(this.$els.toggle, 'not-clickable')) {
      removeClass(this.$els.toggle, 'not-clickable')
    }

    if (!this.options.withController) {
      this.setEvents()
    }

    if (!this.$els.arrow) {
      this.setArrow()
    }

    this.on('change:isOpen', this.toggleDropdown.bind(this))
    this.setState('height', this.$els.menu.clientHeight)

    this.$el.setAttribute('data-dropdown', '')
    this.$els.toggle.setAttribute('data-dropdown-toggle', '')
    this.$els.menu.setAttribute('data-dropdown-menu', '')

    TweenMax.set(this.$els.menu, { height: 0 })
  }

  getDefaultOptions() {
    return { withController: false }
  }

  setInitialState() {
    return {
      isOpen: false
    }
  }

  bindStateEvents() {
    return {
      isOpen: (val) => {
        this.toggleDropdown(val)
      }
    }
  }

  setArrow() {
    this.$els.arrow = stringToDOM('<svg class="dropdown-arrow o-icon o-icon--arrow"><use xlink:href="#arrow-down"/></svg>')
    this.$els.toggle.appendChild(this.$els.arrow)
  }

  setEvents() {
    this.$ev.on(this.$els.toggle, 'click', (e) => {
      e.preventDefault()
      e.stopPropagation()
      this.setState('isOpen', !this.getState('isOpen'))
    })

    Object.values(this.$refs).forEach(listener => {
      listener.on('dropdown:opening', (current) => {
        const others = Object.values(this.$refs).filter(filter => current.el !== filter.el)
        others.forEach(child => child.toggleDropdown(false, true))
      })
    })

    this.on('dropdown:closing', () => {
      Object.values(this.$refs).forEach(current => current.toggleDropdown(false, true))
    })
  }

  toggleDropdown(val, force = false) {
    if (this.getState('isAnimating') && !force) return

    const tl = new TimelineMax({ onComplete: () => { this.setState('isAnimating', false) } })
    this.setState('isAnimating', true)
    this.setState('isOpen', val)

    if (val) {
      tl
        .set([this.$el, this.$els.arrow], { className: '+=is-open' })
        .set(this.$els.menu, { autoAlpha: 1 })
        .to(this.$els.menu, 0.8, { height: this.getState('height'), ease: Expo.easeInOut, clearProps: 'all' })
        .staggerFromTo(this.$els.menu.children, 0.6, { autoAlpha: 0 }, { autoAlpha: 1 }, 0.1)

      this.emit('dropdown:opening', this)

    } else {
      tl
        .add('start')
        .to(this.$els.menu.children, 0.4, { autoAlpha: 0 }, 'start')
        .to(this.$els.menu, 0.8, { height: 0, ease: Expo.easeInOut }, 'start')
        .set(this.$els.menu.children, { clearProps: 'all' })
        .set([this.$el, this.$els.arrow], { className: '-=is-open' })

      this.emit('dropdown:closing', this)
    }
  }
}

export default Dropdown