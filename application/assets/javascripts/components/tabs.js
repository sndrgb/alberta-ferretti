import { TweenMax, TimelineMax } from 'gsap/all'

import Panel from './panel'
import { qs, qsa, addClass, removeClass } from '../base/utils'

class Tabs extends Panel {

  mounted() {
    super.mounted()

    this.$els.tabs = qsa('[data-tab]', this.$el)
    this.$els.contents = qsa('[data-tab-content]', this.$els.wrapper)
    this.$els.close = qs('#close-tabs', this.$els.wrapper)
    this.$els.tabs.push(...qsa('[data-tab]', this.$els.wrapper))

    this.toggleTabs = this.toggleTabs.bind(this)
    this.on('change:activeTab', this.toggleTabs)

    TweenMax.set(this.$els.contents, { autoAlpha: 0 })

    this.$els.tabs.forEach(el => {
      this.$ev.on(el, 'click', () => {
        if (this.getState('isTabsAnimating')) return
        this.setState('activeTab', el.getAttribute('data-tab'))
      })
    })
  }

  setInitialState() {
    return {
      isTabsAnimating: false,
      activeTab: null
    }
  }

  toggleTabs(val, oldval) {
    const tl = new TimelineMax({ onComplete: () => { this.setState('isTabsAnimating', false) } })
    this.setState('isTabsAnimating', true)

    this.$els.activeTab = qs(`[data-tab-content="${val}"]`, this.$els.wrapper)
    const oldTab = qs(`[data-tab-content="${oldval}"]`, this.$els.wrapper)

    const activeToggle = this.$els.toggles.filter(el => el.getAttribute('data-tab') === val)
    const oldToggle = this.$els.toggles.filter(el => el.getAttribute('data-tab') === oldval)

    activeToggle.forEach(el => addClass(el, 'is-active'))
    if (oldToggle) {
      oldToggle.forEach(el => removeClass(el, 'is-active'))
    }

    if (oldTab) {
      tl.to(oldTab, 0.3, { autoAlpha: 0 })
    }

    tl.to(this.$els.activeTab, 0.3, { autoAlpha: 1 })
  }
}

export default Tabs