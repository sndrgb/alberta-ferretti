import { TweenMax, TimelineMax, Expo } from 'gsap/all'
import { Component } from 'yuzu'
import inViewport from 'in-viewport'

import Slideer from './slideshows/slideer'
import RafScroll from './scrollbar/scroll'
import { qs, qsa, isMobile, addClass } from '../base/utils'

class Hero extends Component {

  mounted() {
    this.$els.hovers = qsa('[data-hover]', this.$el)
    this.$els.backgrounds = qsa('[data-background]', this.$el)

    TweenMax.set(this.$els.backgrounds, { autoAlpha: 0 })
    TweenMax.set(this.$els.hovers[0], { className: '+=is-active' })
    TweenMax.set(this.$els.backgrounds[0], { autoAlpha: 1, scale: 1.1 })

    if (this.$el.getBoundingClientRect().top === 0) {
      addClass(this.$el, 'is-top-hero')

      if (!isMobile()) {
        RafScroll.add(this.scrollHero.bind(this))
      }
    }

    const video = qsa('[data-autoplay]')
    if (video.length) {
      video.forEach(el => el.play())
    }

    this.setEvents()
  }

  setEvents() {
    if (this.$els.hovers.length === 1) return

    if (isMobile()) {
      this.initMobileSlideshow()
    } else {
      this.$els.hovers.forEach((el, i) => {
        this.$ev.on(el, 'mouseenter', this.enterBackground.bind(this, i, el))
        this.$ev.on(el, 'mousemove', this.enterBackground.bind(this, i, el))
      })
    }

  }

  getInitialState() {
    return {
      oldId: 0,
      isAnimating: false
    }
  }

  scrollHero() {
    if (!inViewport(this.$el)) return
    const bounds = this.$el.getBoundingClientRect()

    if (window.scrollY < bounds.height) {
      const offset = bounds.top + bounds.height
      const delta = Math.min(offset / bounds.height, 1)
      const translateY = -(1 - delta) * 400

      if (delta > 0) {
        TweenMax.set(this.$el, {
          y: -translateY
        })
      }
    }
  }

  enterBackground(i, el) {
    if (this.getState('isAnimating')) return
    if (this.getState('oldId') === i) return

    this.setState('isAnimating', true)

    const oldBack = this.$els.backgrounds[this.getState('oldId')]
    const oldEl = this.$els.hovers[this.getState('oldId')]
    const newBack = this.$els.backgrounds[i]

    const tl = new TimelineMax({
      onComplete: () => {
        this.setState('oldId', i)
        this.setState('isAnimating', false)
      }
    })

    tl
      .set(el, { className: '+=is-active' })
      .set(oldEl, { className: '-=is-active' })
      .add('start')
      .to(oldBack, 1.6, {
        autoAlpha: 0,
        scale: 1.3,
        zIndex: 0,
        ease: Expo.easeInOut,
        className: '-=is-active'
      }, 'start')
      .add(() => {
        oldBack.pause()
        newBack.play()
      }, 'start')
      .fromTo(newBack, 1.6, {
        autoAlpha: 0,
        scale: 1.3,
        zIndex: 1
      }, {
        autoAlpha: 1,
        scale: 1,
        zIndex: 1,
        className: '+=is-active',
        ease: Expo.easeInOut
      }, 'start')
  }

  initMobileSlideshow() {
    this.setRef({
      el: qs('.c-hero__wrapper', this.$el),
      component: Slideer,
      id: 'slideshow',
      opts: {
        length: this.$els.hovers.length,
        loop: true,
        counter: true,
        callback: (event) => this.slideshowCallback(event)
      }
    })

    TweenMax.set(this.$els.hovers, { autoAlpha: 0 })
    TweenMax.set(this.$els.hovers[0], { autoAlpha: 1 })
  }

  slideshowCallback(event) {
    const index = event.current

    const tl = new TimelineMax({
      paused: true,
      onComplete: () => {
        this.$refs.slideshow.animating = false
      }
    })

    tl
    .add('start')
    .staggerTo(this.$els.hovers, 1, {
      cycle: {
        x: loop => { return index === loop ? 0 : -window.innerWidth * event.direction },
        autoAlpha: loop => { return index === loop ? 1 : 0 },
        zIndex: loop => { return index === loop ? 3 : 2 }
      },
      ease: Expo.easeInOut
    }, 0, 'start')
    .staggerTo(this.$els.backgrounds, 1, {
      cycle: {
        autoAlpha: loop => { return index === loop ? 1 : 0 },
        zIndex: loop => { return index === loop ? 1 : 0 }
      },
      ease: Expo.easeInOut
    }, 0, 'start')

    tl.restart()
  }
}

export default Hero