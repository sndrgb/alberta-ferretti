/**
 * Main Application File
 *
 * Use for bootstrapping large application
 * or just fill it with JS on small projects
 *
 */

/**
 * This is a test script
 */

import 'whatwg-fetch'

import classie from 'classie'
import Barba from 'barba.js'
import detectIt from 'detect-it'
import { TweenMax, Expo } from 'gsap/all'

import { data, qsa, qs, removeClass, isMobile } from './base/utils'
import Menu from './components/menu'
import MenuMobile from './components/menu-mobile'
import Newsletter from './components/newsletter'
import { GenericTransition } from './transitions'

// MAYBE YOU REMOVE THIS
import './components/text-search'

// NAMESPACES
import Generic from './sections/generic'
import './sections/home'
import './sections/listing'
import './sections/product'
import './sections/boutique'
import './sections/mood'
import './sections/collection'
import './sections/adcampaign'

new Generic()

if (!detectIt.hasTouch) {
  classie.addClass(document.documentElement, 'no-touchevents')
}

const links = qsa('a[href]', document)
links.forEach((el) => {
  el.addEventListener('click', (e) => {
    if (e.currentTarget.href === window.location.href) {
      e.preventDefault()
      e.stopPropagation()
    }
  })
})

document.addEventListener('DOMContentLoaded', () => {
  const menuVoices = qsa('[data-nav]')
  const getMenuVoice = (els, namespace) => els.find(el => el.getAttribute('data-namespace', namespace) === namespace)
  let lastElementClicked
  let currentNamespace

  // start up
  Barba.Pjax.start()
  Barba.Prefetch.init()

  const newsletter = new Newsletter('#newsletter-modal').init()
  const menuDesktop = new Menu('#header')
  const menuMobile = new MenuMobile('#menu-mobile')
  const $loader = qs('#loader')
  const AfterLoadCallback = () => {
    if (isMobile()) {
      menuMobile.init()
    } else {
      menuDesktop.init()
    }
    newsletter.trigger()
    document.body.removeChild($loader)
  }

  TweenMax.to($loader, 2, {
    yPercent: -100,
    ease: Expo.easeInOut,
    onComplete: AfterLoadCallback
  })

  // get current namespace
  Barba.Dispatcher.on('linkClicked', (el) => {
    lastElementClicked = el
    currentNamespace = data('namespace', lastElementClicked)
  })

  // set class to current menu voice
  Barba.Dispatcher.on('newPageReady', (view) => {
    removeClass(document.body, 'is-header-light')

    if (menuDesktop.getState('isOpen')) {
      menuDesktop.closeMenu()
    }

    if (menuMobile.getState('isOpen')) {
      menuMobile.setState('isOpen', false)
    }

    const currentMenuVoice = getMenuVoice(menuVoices, view.namespace)
    if (currentMenuVoice !== undefined && currentMenuVoice !== null) {
      classie.add(currentMenuVoice, 'is-active')
    }
  })

  // get right transition
  Barba.Pjax.getTransition = () => {
    // let transition
    const prevNamespace = Barba.Pjax.History.prevStatus().namespace
    const prevMenu = getMenuVoice(menuVoices, prevNamespace)

    if (prevMenu !== undefined && prevMenu !== null) {
      classie.remove(prevMenu, 'is-active')
    }

    return GenericTransition
  }
})