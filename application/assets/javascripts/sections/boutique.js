import GoogleMapsLoader from 'google-maps'
import { TweenMax, Expo, TimelineMax } from 'gsap/all'

import styles from '../maps.json'
import Slideer from '../components/slideshows/slideer'
import { isMobile, qs, qsa, addClass, toggleClass } from '../base/utils'
import Generic from './generic'

class Boutique extends Generic {

  constructor() {
    super()
    this.namespace = 'boutique'
  }

  onEnter() {
    super.onEnter()
    addClass(document.body, 'is-boutique')

    if (!isMobile()) {
      GoogleMapsLoader.KEY = 'AIzaSyBV8T0tS_BPr8k3LhAlT-2sXyW9BG9jY4s'
      GoogleMapsLoader.LIBRARIES = ['geometry', 'places']
      GoogleMapsLoader.load(this.initMaps.bind(this))
    }

    this.initSlideshow()
  }

  initSlideshow() {
    const $slideshow = qs('#locations-slideshow', this.container)
    const slides = qsa('[data-slide]', $slideshow)
    this.slideshow = new Slideer($slideshow, {
      length: slides.length,
      loop: true,
      callback: (event) => this.slideshowCallback(event, slides)
    }).init()

    TweenMax.set(slides, {
      autoAlpha: 0,
      scale: 1.2
    })

    TweenMax.set(slides[this.slideshow.getCurrentSlide()], {
      autoAlpha: 1,
      scale: 1
    })

    this.slideshowInterval = setInterval(() => {
      const nextIndex = this.slideshow.getCurrentSlide() + 1
      this.slideshow.goTo(nextIndex)
    }, 5000)
  }

  slideshowCallback(event, slides) {
    const index = event.current
    const prev = event.previous
    this.slideshow.animating = true

    const tl = new TimelineMax({
      paused: true,
      onComplete: () => {
        this.slideshow.animating = false
      }
    })

    tl.staggerTo(slides, 3, {
      cycle: {
        scale: loop => { return index === loop ? 1 : 1.2 },
        autoAlpha: loop => { return index === loop ? 1 : 0 },
        zIndex: loop => { return index === loop ? 1 : 0 }
      },
      ease: Expo.easeInOut
    }, 0, 0)

    tl.restart()
  }

  initMaps(google) {
    const $map = qs('#map', this.container)
    const placeId = $map.getAttribute('data-map-id')
    const icon = $map.getAttribute('data-pin')
    const map = new google.maps.Map($map, {
      zoom: 16,
      styles,
      disableDefaultUI: true
    })
    const request = { placeId }

    function callback(place, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        const marker = new google.maps.Marker({
          map,
          icon,
          position: place.geometry.location
        })

        map.setCenter(marker.getPosition())
      }
    }

    const service = new google.maps.places.PlacesService(map)
    service.getDetails(request, callback)
  }

  onLeave() {
    super.onLeave()
    clearInterval(this.slideshowInterval)
    this.slideshow.destroy()
    toggleClass(document.body, 'is-boutique')
  }
}

new Boutique()