import { addClass, toggleClass } from '../base/utils'
import Generic from './generic'
import Instagram from '../components/instagram'

class Homepage extends Generic {

  constructor() {
    super()
    this.namespace = 'home'
  }

  onEnter() {
  }

  onEnterCompleted() {
    super.onEnterCompleted()
    addClass(document.body, 'is-home')

    this.instagram = new Instagram('#instagram-layer')
  }

  onLeave() {
    super.onLeave()

    this.instagram.destroy()
    toggleClass(document.body, 'is-home')
  }
}

new Homepage()