import { addClass, toggleClass } from '../base/utils'
import Generic from './generic'

class Collection extends Generic {

  constructor() {
    super()
    this.namespace = 'collection'
  }

  onEnter() {
    super.onEnter()
    addClass(document.body, 'is-collection')
  }

  onLeave() {
    super.onLeave()
    toggleClass(document.body, 'is-collection')
  }
}

new Collection()