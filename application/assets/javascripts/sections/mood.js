import { addClass, toggleClass } from '../base/utils'
import Generic from './generic'

class Mood extends Generic {

  constructor() {
    super()
    this.namespace = 'mood'
  }

  onEnter() {
    super.onEnter()
    addClass(document.body, 'is-mood')
  }

  onLeave() {
    super.onLeave()
    toggleClass(document.body, 'is-mood')
  }
}

new Mood()