import Notify from '../components/notify-me'
import { qs, addClass, toggleClass, qsa, isMobile } from '../base/utils'
import Generic from './generic'
import SlideshowManager from '../components/slideshows/slideshow-manager'

class Product extends Generic {

  constructor() {
    super()
    this.namespace = 'product'
  }

  onEnter() {
    super.onEnter()
    const options = qsa('.c-options-product', this.container)
    addClass([...options].pop(), 'is-last')

    const $notifyme = qs('.js-notify-me')
    if ($notifyme) {
      this.notify = new Notify($notifyme).init()
    }

    if (isMobile()) {
      this.carousel = new SlideshowManager('.js-mobile-carousel', { type: 'fade' }).init()
    }
  }

  onEnterCompleted() {
    super.onEnterCompleted()
    addClass(document.body, 'is-product')
  }

  onLeave() {
    super.onLeave()
    toggleClass(document.body, 'is-product')
  }
}

new Product()