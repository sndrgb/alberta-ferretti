import Barba from 'barba.js'
import { qsa, qs } from '../base/utils'

import Hero from '../components/hero'
import Player from '../components/player'
import NewsletterSubscribe from '../components/newsletter-subscribe'
import NeedHelp from '../components/need-help'
import QuickBuy from '../components/quick-buy'
import Dropdown from '../components/dropdown'
import Panel from '../components/Panel'
import Filters from '../components/filters'
import ZoomGallery from '../components/zoom-gallery/index'
import Gallery from '../components/gallery/index'
import Tabs from '../components/tabs'
import MultiCarousel from '../components/multi-carousel'
import SlideshowManager from '../components/slideshows/slideshow-manager'
import Smooth from '../components/scrollbar/scrollbar'

class Generic {

  constructor() {
    this.namespace = 'generic'
    this.init()
  }

  init() {
    Barba.Dispatcher.on('initStateChange', (newStatus, oldStatus) => {
      if (oldStatus && oldStatus.namespace === this.namespace) this.onLeave()
    })

    Barba.Dispatcher.on('newPageReady', (newStatus, oldStatus, container) => {
      this.container = container
      if (newStatus.namespace === this.namespace) this.onEnter()
    })

    Barba.Dispatcher.on('transitionCompleted', (newStatus, oldStatus) => {
      if (newStatus.namespace === this.namespace) this.onEnterCompleted()
      if (oldStatus && oldStatus.namespace === this.namespace) this.onLeaveCompleted()
    })
  }

  onEnter() {}

  onEnterCompleted() {
    // bind hero component
    const $heroes = qsa('[data-hero]', this.container)
    $heroes.forEach(el => {
      new Hero(el).init()
    })

    qsa('.js-dropdown', document).forEach(el => { new Dropdown(el).init() })

    // bind slideshows
    const $slideshows = qsa('[data-slideshow]', this.container)
    $slideshows.forEach(el => {
      const type = el.getAttribute('data-slideshow')
      new SlideshowManager(el, { type }).init()
    })

    // bind scroll listener
    this.smooth = new Smooth()

    if (qs('[data-panels="gallery"]')) {
      this.gallery = new Gallery(this.container, { zoom: false }).init()
    }

    if (qs('[data-newsletter-footer]', this.container)) {
      this.newsletterSubscribe = new NewsletterSubscribe(qs('[data-newsletter-footer]', this.container)).init()
    }

    // bind components
    const $components = qsa('[data-component]')
    this.$components = []
    $components.forEach(el => {
      const type = el.getAttribute('data-component')
      let component

      if (type === 'quick-buy') {
        component = new QuickBuy(el).init()
      } else if (type === 'multi-carousel') {
        component = new MultiCarousel(el).init()
      } else if (type === 'video-player') {
        component = new Player(el).init()
      } else if (type === 'gallery-zoom') {
        component = new ZoomGallery(el).init()
      } else if (type === 'need-help') {
        component = new NeedHelp(el).init()
      } else if (type === 'filters') {
        component = new Filters(el).init()
      } else if (type === 'panel') {
        component = new Panel(el).init()
      } else if (type === 'tabs') {
        component = new Tabs(el).init()
      }

      this.$components.push(component)
    })
  }

  onLeave() {
    this.$components.forEach(el => {
      if (el.destroy) {
        el.destroy()
      }
    })

    if (this.gallery) {
      this.gallery.destroy()
    }

    if (this.newsletterSubscribe) {
      this.newsletterSubscribe.destroy()
    }
  }
  onLeaveCompleted() {}
}

export default Generic