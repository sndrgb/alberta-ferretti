import { addClass, toggleClass } from '../base/utils'
import Generic from './generic'

class ADCampaign extends Generic {

  constructor() {
    super()
    this.namespace = 'adcampaign'
  }

  onEnter() {
    super.onEnter()
    addClass(document.body, 'is-adcampaign')
  }

  onLeave() {
    super.onLeave()
    toggleClass(document.body, 'is-adcampaign')
  }
}

new ADCampaign()