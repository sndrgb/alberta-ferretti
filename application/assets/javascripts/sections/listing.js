import { TweenMax, ScrollToPlugin } from 'gsap/all'
import { qsa, addClass, toggleClass, handleEvent } from '../base/utils'

import Generic from './generic'

const st = [ScrollToPlugin]

class Listing extends Generic {

  constructor() {
    super()
    this.namespace = 'listing'
  }

  onEnter() {
    const $anchors = qsa('[data-anchor]', document)
    this.anchorLinks = []
    $anchors.forEach(el => {
      const event = handleEvent('click', {
        onElement: el,
        withCallback: (e) => {
          e.preventDefault()
          e.stopPropagation()

          const scrollTo = {
            y: `#${el.getAttribute('data-anchor')}`,
            offsetY: 150
          }
          TweenMax.to(window, 1.2, { scrollTo })
        }
      })

      this.anchorLinks.push(event)
    })
  }

  onEnterCompleted() {
    super.onEnterCompleted()
    addClass(document.body, 'is-listing')
  }

  onLeave() {
    super.onLeave()

    toggleClass(document.body, 'is-listing')
  }
}

new Listing()