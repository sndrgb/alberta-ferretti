import Barba from 'barba.js';

import { qsa } from '../base/utils';
import ins from './inAnimations';
import outs from './outAnimations';

export default Barba.BaseTransition.extend({
    name: 'Home to Others',
    isValid(prev, next) {
        const valid = (prev === 'home' && next !== 'home') || (prev === 'home' && next !== 'work');
        this.current = next;
        return valid;
    },

    start() {
        const current = this.current === 'work' ? 'stagger' : 'generic';
        Promise.all([
            this.newContainerLoading,
            outs.generic.bind(this)()
        ]).then(ins[current].bind(this));
    },

    setNewElement() {
        this.elsIn = qsa('[data-animation="out"]', this.newContainer);
    }
});