import { TimelineMax, TweenMax } from 'gsap/all'

const transitions = {
  generic() {
    window.scrollTo(0, 0)

    TweenMax.fromTo(this.newContainer, 0.5, {
      position: 'absolute',
      top: 0,
      right: 0,
      left: 0,
      autoAlpha: 0
    }, {
      autoAlpha: 1,
      clearProps: 'all',
      onComplete: () => { this.done() }
    })
  },

  stagger() {
    const tl = new TimelineMax({ onComplete: () => { this.done() } })
    this.setNewElement()

    tl.add('start')
    .fromTo(this.newContainer, 0.1, {
      autoAlpha: 0
    }, {
      autoAlpha: 1
    }, 'start')
    .staggerTo(this.elsIn, 2, {
      autoAlpha: 1
    }, 0.2)
  }
}

export default transitions