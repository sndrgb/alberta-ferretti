import { TimelineMax } from 'gsap/all'

const transitions = {
  generic() {
    return new Promise((resolve) => {
      const tl = new TimelineMax({ onComplete: () => { resolve() } })

      tl.to(this.oldContainer, 0.5, {
        position: 'fixed',
        top: 0,
        right: 0,
        left: 0,
        autoAlpha: 0
      })
    })
  },

  stagger() {
    return new Promise((resolve) => {
      const tl = new TimelineMax({ onComplete: () => { resolve() } })
      this.setElements()

      tl.add('start')
      .staggerTo(this.elsOut, 1, {
        autoAlpha: 0
      }, 0.1)
      .set(this.oldContainer, {
        autoAlpha: 0
      })
    })
  }
}

export default transitions