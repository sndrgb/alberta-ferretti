/**
 * Grunt build tasks
 */

/*eslint-env node */
/*eslint one-var: 0, no-new: 0, func-names: 0, strict: 0, import/no-extraneous-dependencies: 0 */

const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const _ = require('lodash');
const $ = require('gulp-load-plugins')();
const argv = require('yargs').argv || {};
const pkg = require('./package.json');
const paths = require('./build/gulp-config/paths');

const taskPath = path.join(process.cwd(), 'build', 'gulp-tasks');
const optionsPath = path.join(process.cwd(), 'build', 'gulp-config');
const options = {};
const banners = {};

const logError = (...args) => $.util.log($.util.colors.red(...args));

pkg.year = new Date().getFullYear();
/* eslint-disable */
banners.application = "/*! <%= pkg.description %> v<%= pkg.version %> - <%= pkg.author.name %> - Copyright <%= pkg.year %> <%= pkg.author.company %> */\n";
banners.vendors = "/*! <%= pkg.description %> v<%= pkg.version %> - <%= pkg.author.name %> - Vendor package */\n";
/* eslint-enable */

//load configuration from files
['hosts', 'properties'].forEach((filename) => {
    const configFilePath = path.join(optionsPath, `${filename}.js`);
    const configLocalFilePath = path.join(optionsPath, `${filename}.local.js`);
    let conf = {};

    if (fs.existsSync(configFilePath)) {
        conf = _.assign(conf, require(configFilePath));
    }

    if (fs.existsSync(configLocalFilePath)) {
        conf = _.merge(conf, require(configLocalFilePath));
    }

    if (filename === 'properties') {
        _.assign(options, conf);
    } else {
        options[filename] = conf;
    }
});

if (fs.existsSync(path.join(optionsPath, 'paths.local.js'))) {
    paths.merge(require(path.join(optionsPath, 'paths.local.js')));
}

if (_.has(argv, 'remotehost')) {
    logError('WARNING: param `--remotehost` is deprecated use `--target` instead');
    argv.target = argv.remotehost;
}

_.forOwn({
    production: false,
    command: null,
    target: null //be explicit!
}, (value, key) => {
    options[key] = _.has(argv, key) ? argv[key] : value;
});

//force production env
if (options.production) process.env.NODE_ENV = 'production';
if (options.target === null) options.target = options.production ? 'production' : 'development';
options.pkg = pkg;
options.banners = banners;

//unique build identifier
options.buildHash = `buildhash${(Date.now())}`;

options.getDeployTarget = (target) => {
    const targets = Object.keys(options.hosts).filter((host) => !!options.hosts[host].host);
    if (!target || targets.indexOf(target) === -1) {
        logError('ERROR: Deploy target unavailable. Specify it via `--target` argument. Allowed targets are: ' + targets.join(', ')); //eslint-disable-line
        return false;
    }
    return options.hosts[target];
};

fs.readdirSync(taskPath).filter((taskFile) => path.extname(taskFile) === '.js').forEach((taskFile) => {
    require(`${taskPath}/${taskFile}`)(gulp, $, options); //eslint-disable-line global-require
});

const tasks = [
    'clean',
    'images',
    gulp.parallel('fonts', 'media', 'styles', 'scripts'),
    'modernizr',
    'views'
];

if (options.styleguideDriven) tasks.push('styleguide');
if (options.production) tasks.push('rev', 'clean:tmp');
if (options.production) tasks.push('rev', 'clean:tmp');
gulp.task('default', gulp.series(...tasks, (done) => { done(); }));
gulp.task('dev', gulp.series('default', 'serve'));


gulp.task('deploy', (done) => {
    const host = options.getDeployTarget(options.target);
    const deployStrategy = host.deployStrategy || options.deployStrategy;

    if (!deployStrategy) {
        $.util.warn('No deploy strategy set as default or in the host config');
        done();
        return;
    }

    switch (deployStrategy) {
    case 'rsync':
        //force backup
        options.command = 'backup';
        tasks.push('remote', 'rsync');
        break;
    default:
        tasks.push(deployStrategy);
        break;
    }
    tasks.push(done);

    gulp.series(...tasks);
});
