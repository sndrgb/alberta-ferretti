const gulp = require('gulp')
const svgSprite = require('gulp-svg-sprites')

gulp.task('default', () => {
  return gulp.src('assets/*.svg')
      .pipe(svgSprite({mode: "symbols"}))
      .pipe(gulp.dest("out"))
})