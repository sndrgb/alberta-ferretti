module.exports = {
    deployStrategy: 'rsync', //`rsync` or `ftp`
    livereload: true, //set to `true` to enable livereload
    styleguideDriven: false, //will rebuild the styleguide whenever stylesheets change
    viewmatch: '*.html' //for php projects use: '*.{html,php,phtml}'
};